---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 推送规则 **(PREMIUM)**

通过使用正则表达式，拒绝基于提交内容、分支名称或文件详细信息的推送，获得对能和不能推送到仓库的内容的额外控制。

极狐GitLab 已经提供了[受保护的分支](../user/project/protected_branches.md)，但有些情况下您需要一些特定的规则。常见场景：防止 Git 标签移除，或强制提交消息的特殊格式。

推送规则是您可以在用户友好的界面中启用。[pre-receive Git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)， 它们被定义为：

- 如果您是管理员，则全局适用。
- 在每个项目中，因此您可以根据需要将不同的规则应用于不同的项目。

## 用例

每个推送规则都可以有自己的用例，但让我们考虑一些示例。

### 使用特定参考提交消息

假设您对工作流程有以下要求：

- 每个提交都应该引用一个 Jira 议题，例如：`重构的 css。修复 JIRA-123。`
- 用户不应该使用 `git push` 删除 Git 标签

编写一个需要在提交消息中提及 Jira 议题的正则表达式，例如 `JIRA\-\d+`。

现在，当用户尝试使用消息 `Bugfix` 推送提交时，他们的推送会被拒绝。只接受推送带有 `Bugfix according to JIRA-123` 之类的消息的提交。

### 限制分支名称

如果您的公司对分支名称有严格的政策，您可能希望分支以特定名称开头。这种方法支持依赖分支名称的不同 GitLab CI/CD 作业（例如`feature`、`hotfix`、`docker`、`android`）。

您的开发人员可能不记得该策略，因此他们可能会推送到各个分支，并且 CI 流水线可能无法按预期工作。通过在推送规则中全局限制分支名称，可以防止此类错误。
所有与推送规则不匹配的分支名称都将被拒绝。

请注意，无论指定的分支命名正则表达式（regex）如何，始终允许使用默认分支的名称。以这种方式配置，因为合并通常将默认分支作为其目标。
如果您有其他目标分支，请将它们包含在您的正则表达式中。（参见[启用推送规则](#启用推送规则)）。

默认分支也默认为[受保护的分支](../user/project/protected_branches.md)，这已经限制了用户直接推送。

您可以在推送规则中使用的一些示例正则表达式：

- `^JIRA-` 分支必须以 `JIRA-` 开头。
- `-JIRA$` 分支必须以 `-JIRA` 结尾。
- `^[a-z0-9\\-]{4,15}$` 分支长度必须在 `4` 到 `15` 个字符之间，只接受小写字母、数字和破折号。

#### 默认限制分支名称

<!--
> Introduced in GitLab 12.10.
-->

默认情况下，极狐GitLab 出于安全目的限制某些格式的分支名称。禁止使用类似于 Git 提交哈希的 40 个字符的十六进制名称。

### 自定义推送规则 **(PREMIUM SELF)**

通过使用更高级的服务器钩子，可以创建自定义推送规则而不是 **管理中心 > 推送规则** 中可用的推送规则。

<!--
See [server hooks](../administration/server_hooks.md) for more information.
-->

## 启用推送规则

您可以为所有要继承的新项目创建推送规则，但可以在项目级别或[群组级别](../user/group/index.md#群组推送规则)覆盖它们。

创建全局推送规则：

1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏上，选择 **推送规则**。

要覆盖项目设置中的全局推送规则：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **推送规则**。
1. 设置您想要的规则。
1. 选择 **保存推送规则**。

以下选项可用：

| 推送规则                      | 描述 |
|---------------------------------|-------------|
| 使用 `git push` 删除标签 | 禁止用户使用 `git push` 删除 Git 标签。 可以通过 Web UI 删除标签。 |
| 检查提交作者是否为 GitLab 用户 | 限制对现有 GitLab 用户的提交（根据他们的电子邮件进行检查）。 <sup>1</sup> |
| 拒绝未经验证的用户 | GitLab 拒绝任何不是由推送它的用户的提交，或者提交者的电子邮件地址不是已确认<!--[已确认](../security/user_email_confirmation.md)-->。 |
| 检查提交是否通过 GPG 签名 | 拒绝未通过 GPG 签名的提交。阅读[使用 GPG 签名提交](../user/project/repository/gpg_signed_commits/index.md)。 |
| 防止推送 secret 文件 | GitLab 拒绝任何可能包含 secret 的文件。请参阅[禁止的文件名](#防止将-secret-推送到仓库)。 |
| 提交消息中的必需表达式 | 只允许推送与此正则表达式匹配的提交消息。 <sup>2</sup> 留空以允许任何提交消息。使用多行模式，可以使用`(?-m)` 禁用。 |
| 提交消息中的拒绝表达式 | 只允许推送与此正则表达式不匹配的提交消息。 <sup>2</sup> 留空以允许任何提交消息。 使用多行模式，可以使用`(?-m)` 禁用。 |
| 按分支名称限制 | 只允许推送与此正则表达式匹配的分支名称。 <sup>2</sup> 留空以允许所有分支名称。 |
| 通过提交作者的电子邮件限制 | 只允许推送与此正则表达式匹配的提交作者的电子邮件。 <sup>1</sup> <sup>2</sup> 留空以允许任何电子邮件。 |
| 禁止的文件名 | 不允许推送任何与此正则表达式匹配且不存在于仓库中的已提交文件名。 <sup>2</sup> 留空以允许任何文件名。请参阅[常见示例](#禁止的文件名)。 |
| 最大文件大小 | 包含超过此文件大小（以 MB 为单位）的添加或更新文件的推送将被拒绝。设置为 0 以允许任何大小的文件。Git LFS 跟踪的文件被豁免。 |

1. 检查提交作者和提交者。
1. GitLab 的推送规则中的正则表达式使用 [RE2 语法](https://github.com/google/re2/wiki/Syntax) ，您可以在 [regex101 regex tester](https://regex101.com/) 中测试。

### 注意“拒绝未签名的提交”推送规则

此推送规则会忽略由 GitLab（通过 UI 或 API）验证和创建的提交。当启用 **拒绝未签名提交** 推送规则时，如果提交是在 **GitLab 本身内** 创建的，则未签名提交可能仍会显示在提交历史记录中。正如预期的那样，在 GitLab 之外创建并推送到仓库的提交被拒绝。<!--For more information about how GitLab
plans to fix this issue, read [issue #19185](https://gitlab.com/gitlab-org/gitlab/-/issues/19185).-->

#### “拒绝未签名的提交”推送规则禁用 Web IDE

在 13.10 版本中，如果项目具有“拒绝未签名的提交”推送规则，将不允许用户通过 GitLab Web IDE 进行提交。

要允许通过 Web IDE 在具有此推送规则的项目上提交，GitLab 管理员需要禁用功能标志 “reject_unsigned_commits_by_gitlab”。通过 <!--[rails 控制台](../administration/operations/rails_console.md)-->rails 控制台并运行：

```ruby
Feature.disable(:reject_unsigned_commits_by_gitlab)
```

## 防止将 secret 推送到仓库

> 移动到专业版于 13.9 版本。

凭据文件、SSH 私钥和其他包含机密的文件等机密永远不应提交给源代码控制。
极狐GitLab 使您可以打开无法推送到仓库的文件的预定义拒绝列表。该列表阻止这些提交到达远程存储库。

通过选中复选框 *Prevent committing secrets to Git*，当文件与从 [`files_denylist.yml`](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/ee/lib/gitlab/checks/files_denylist.yml)（查看此文件时，请确保您位于正确的分支作为 GitLab 版本）。

NOTE:
已提交的文件不受此推送规则的限制。

以下是极狐GitLab 使用这些正则表达式拒绝的示例列表：

```shell
#####################
# AWS CLI credential blobs
#####################
.aws/credentials
aws/credentials
homefolder/aws/credentials

#####################
# Private RSA SSH keys
#####################
/ssh/id_rsa
/.ssh/personal_rsa
/config/server_rsa
id_rsa
.id_rsa

#####################
# Private DSA SSH keys
#####################
/ssh/id_dsa
/.ssh/personal_dsa
/config/server_dsa
id_dsa
.id_dsa

#####################
# Private ed25519 SSH keys
#####################
/ssh/id_ed25519
/.ssh/personal_ed25519
/config/server_ed25519
id_ed25519
.id_ed25519

#####################
# Private ECDSA SSH keys
#####################
/ssh/id_ecdsa
/.ssh/personal_ecdsa
/config/server_ecdsa
id_ecdsa
.id_ecdsa

#####################
# Any file with .pem or .key extensions
#####################
*.pem
*.key

#####################
# Any file ending with _history or .history extension
#####################
*.history
*_history
```

## 禁止的文件名

> 移动到专业版于 13.9 版本。

Git 推送中包含的每个文件名都与此字段中的正则表达式进行比较。Git 中的文件名由文件名和它之前的任何目录组成。单个正则表达式可以包含多个用作排除项的独立匹配项。文件名可以广泛匹配仓库中的任何位置，也可以限制为特定位置。文件名也可以是部分匹配项，用于按扩展名排除文件类型。

以下示例使用正则表达式字符串边界字符，它们匹配字符串的开头 (`^`) 和结尾 (`$`)。它们还包括目录路径或文件名可以包含 `.` 或 `/` 的实例。这两个特殊的正则表达式字符都必须使用反斜杠 `\\` 进行转义，才能在匹配条件中用作普通字符。

示例：防止将任何 `.exe` 文件推送到仓库中的任何位置。这是部分匹配的示例，它可以匹配任何结尾包含 `.exe` 的文件名：

```plaintext
\.exe$
```

示例：防止推送仓库根目录中的特定配置文件：

```plaintext
^config\.yml$
```

示例：防止推送已知目录中的特定配置文件：

```plaintext
^directory-name\/config\.yml$
```

示例：防止名为 `install.exe` 的特定文件被推送到仓库中的任何位置。括号表达式`(^|\/)` 匹配目录分隔符后的文件或仓库根目录中的文件：

```plaintext
(^|\/)install\.exe$
```

示例：将上述所有内容组合在一个表达式中。前面的表达式依赖于字符串结尾字符 `$`。我们可以将每个表达式的那部分移动到匹配条件分组集合的末尾，并将其附加到所有匹配项：

```plaintext
(\.exe|^config\.yml|^directory-name\/config\.yml|(^|\/)install\.exe)$
```

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
