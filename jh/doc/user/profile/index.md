---
type: index, howto
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 用户帐户 **(FREE)**

每个帐户都有用户资料，其中包含有关您和您的极狐GitLab 活动的信息。

您的个人资料还包括用于自定义 GitLab 体验的设置。

## 访问您的用户资料

要访问您的用户资料：

1. 在顶部栏的右上角，选择您的头像。
1. 选择您的姓名或用户名。

## 访问您的用户设置

要访问您的用户设置：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。

## 更改您的密码

要更改密码：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **密码**。
1. 在 **当前密码** 文本框中，输入您当前的密码。
1. 在 **新密码** 和 **密码确认** 文本框中，输入您的新密码。
1. 选择 **保存密码**。

如果您不知道当前密码，请选择 **我忘记了密码** 链接。

## 更改您的用户名

您的用户名有一个唯一的[命名空间](../group/index.md#命名空间)，当您更改用户名时会更新。<!--在更改用户名之前，请阅读 [重定向行为的方式](../project/repository/index.md#what-happens-when-a-repository-path-changes)。-->如果您不想更新命名空间，您可以创建一个新用户或群组并将项目转移到其中。

先决条件：

- 您的命名空间不能包含带有 Container Registry<!--[Container Registry](../packages/container_registry/index.md)--> 标签的项目。
- 您的命名空间不能有托管 GitLab Pages <!--[GitLab Pages](../project/pages/index.md)-->的项目。<!--For more information,
  see [this procedure in the GitLab Team Handbook](https://about.gitlab.com/handbook/tools-and-tips/#how-to-change-your-username-at-gitlabcom).-->

要更改您的用户名：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **帐号**。
1. 在 **更改用户名** 部分，输入新用户名作为路径。
1. 选择 **更新用户名**。

## 将电子邮件添加到您的用户个人资料

要将新电子邮件添加到您的帐户：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **电子邮件**。
1. 在 **电子邮件** 文本框中，输入新的电子邮件。
1. 选择 **添加电子邮件地址**。
1. 使用收到的验证电子邮件验证您的电子邮件地址。

## 将您的用户个人资料页面设为私密

您可以使您的用户个人资料仅对您和管理员可见。

要将您的个人资料设为私密：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 选中 **非公开资料** 复选框。
1. 选择 **更新个人资料设置**。

以下内容从您的用户个人资料页面 (`https://gitlab.example.com/username`) 中隐藏：

- Atom feed
- 帐户创建日期
- 活动、群组、参与贡献的项目、个人项目、星标项目和代码片段的标签页。

NOTE:
将您的用户个人资料页面设为私有不会对 REST 或 GraphQL API 隐藏您的公共资源。

### 用户可见性

位于 `/username` 的用户公共页面始终可见，无论您是否登录。

访问用户的公共页面时，您只能看到您有权访问的项目。

如果公开级别受到限制<!--[公开级别受到限制](../admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)-->，则用户个人资料仅对登录用户可见。

## 使用 README 向您的个人资料添加详细信息

> 引入于 14.5 版本。

如果您想向您的个人资料页面添加更多信息，您可以创建一个 README 文件。当您使用信息填充 README 文件时，它会包含在您的个人资料页面中。

要将 README 添加到您的个人资料：

1. 创建一个新的公开项目，项目路径与您的用户名相同。
1. 在这个项目中创建一个 README 文件。该文件可以是任何有效的 README 或索引文件<!--[README 或索引文件](../project/repository/index.md#readme-and-index-files)-->。
1. 使用 Markdown 编写 README 文件。

要使用现有项目，请更新项目的路径<!--[更新项目的路径](../project/settings/index.md#renaming-a-repository)-->以匹配您的用户名。

<!--
## Add external accounts to your user profile page

You can add links to certain other external accounts you might have, like Skype and Twitter.
They can help other users connect with you on other platforms.

To add links to other accounts:

1. On the top bar, in the top-right corner, select your avatar.
1. Select **Edit profile**.
1. In the **Main settings** section, add your information from:
   - Skype
   - LinkedIn
   - Twitter
1. Select **Update profile settings**.
-->

## 在您的用户个人资料页面上显示私人贡献

在用户贡献日历图表和最近活动列表中，您可以看到您对私人项目的贡献活动<!--[贡献活动](../../api/events.md#action-types)-->。

要显示私人贡献：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **主要设置** 部分，选中 **在个人资料中包含非公开贡献** 复选框。
1. 选择 **更新个人资料设置**。

## 添加您的性别代词

> 引入于 14.0 版本。

您可以将性别代词添加到您的帐户，在您的个人资料中，显示在您的姓名旁边。

要指定您的代词：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **Pronouns** 文本框中，输入您的代词。
1. 选择 **更新个人资料设置**。

## 添加您的姓名发音

> 引入于 14.2 版本。

您可以将您的姓名发音添加到您的帐户。显示在您的个人资料中，在您的姓名下方。

要添加您的姓名发音：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **Pronunciation** 文本框中，输入您的姓名的发音方式。
1. 选择 **更新个人资料设置**。

## 设置您的当前状态

> 引入于 13.10 版本，用户可以安排清除他们的状态。

您可以为您的用户个人资料提供自定义状态消息以及描述它的表情符号。
当您不在办公室或因其他原因无法联系时，这可能会有所帮助。

即使您的[个人资料是私密的](#将您的用户个人资料页面设为私密)，您的状态也是公开可见的。

要设置您的当前状态：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **设置状态**，或者，如果您已经设置了状态，则选择 **编辑状态**。
1. 设置所需的表情符号和状态信息。状态消息必须是纯文本且不超过 100 个字符。它们还可以包含表情符号代码，例如“我在度假 :palm_tree:”。
1. 从 **清除状态** 下拉列表中选择一个值。
1. 选择 **设置状态**。或者，您可以选择 **删除状态** 以完全删除您的用户状态。

<!--You can also set your current status by [using the API](../../api/users.md#user-status).-->

如果您选中 **忙碌中** 复选框，请记住在您再次有空时将其清除。

## 设置忙碌状态指示

> - 引入于 13.6 版本。
> - 部署在功能标志后，默认禁用。
> - 于 13.8 版本变为默认启用。
> - 功能标志移除于 13.12 版本。

要向其他人表明您很忙，您可以设置一个指示器。

要设置忙碌状态指示，请执行以下任一操作：

- 直接设置：
   1. 在顶部栏的右上角，选择您的头像。
   1. 选择 **设置状态**，或者，如果您已经设置了状态，则选择 **编辑状态**。
   1. 选中 **忙碌中** 复选框。

- 在您的个人资料中设置：
   1. 在顶部栏的右上角，选择您的头像。
   1. 选择 **编辑个人资料**。
   1. 在 **当前状态** 部分，选中 **忙碌中** 复选框。

   忙碌中状态显示在用户界面中。

<!--
  用户名：

  | Profile page | Settings menu | User popovers |
  | --- | --- | --- |
  | ![Busy status - profile page](img/busy_indicator_profile_page_v13_6.png) | ![Busy status - settings menu](img/busy_indicator_settings_menu_v13_6.png) | ![Busy status - user popovers](img/busy_indicator_user_popovers_v13_6.png) |

  Issue and merge request sidebar:

  | Sidebar| Collapsed sidebar |
  | --- | --- |
  | ![Busy status - sidebar](img/busy_indicator_sidebar_v13_9.png) | ![Busy status - sidebar collapsed](img/busy_indicator_sidebar_collapsed_v13_9.png) |

  Notes:

  | Notes | Note headers |
  | --- | --- |
  | ![Busy status - notes](img/busy_indicator_notes_v13_9.png) | ![Busy status - note header](img/busy_indicator_note_header_v13_9.png) |
-->

## 设置您的时区

您可以将本地时区设置为：

- 在您的个人资料中显示您的当地时间，以及将鼠标悬停在您的姓名上方会显示您的信息的地方。
- 将您的贡献日历与您的当地时间保持一致，以更好地反映您的贡献时间(引入于 14.5 版本)。

要设置您的时区：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **时间设置** 部分，从下拉列表中选择您的时区。

## 更改提交中显示的电子邮件

提交电子邮件是通过极狐GitLab 界面执行的每个 Git 相关操作中显示的电子邮件地址。

您自己的任何经过验证的电子邮件地址都可以用作提交电子邮件。默认情况下使用您的主要电子邮件。

要更改您的提交电子邮件：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **提交邮件** 下拉列表中，选择一个电子邮件地址。
1. 选择 **更新个人资料设置**。

## 更改您的主要电子邮件

您的主要电子邮件：

- 是您登录、提交电子邮件和通知电子邮件的默认电子邮件地址。
- 必须已经[链接到您的用户个人资料](#将电子邮件添加到您的用户个人资料)。

要更改您的主要电子邮件：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **电子邮件** 字段中，输入您的新电子邮件地址。
1. 选择 **更新个人资料设置**。

## 设置您的公开电子邮件

您可以选择您的[已配置电子邮件地址](#将电子邮件添加到您的用户个人资料) 之一以显示在您的公开个人资料中：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **公开邮件** 字段中，选择可用的电子邮件地址之一。
1. 选择 **更新个人资料设置**。

### 使用自动生成的私人提交电子邮件

系统提供了一个自动生成的私人提交电子邮件地址，因此您可以将您的电子邮件信息保密。

要使用私人提交电子邮件：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **提交邮件** 下拉列表中，选择 **使用私人电子邮件**。
1. 选择 **更新个人资料设置**。

每个与 Git 相关的操作都使用私人提交电子邮件。

要保持完全匿名，您还可以复制私人提交电子邮件并使用以下命令在本地计算机上配置它：

```shell
git config --global user.email <your email address>
```

## 故障排查

### 为什么我总是被注销？

当您登录到主应用程序时，会设置一个 `_gitlab_session` cookie。当您关闭浏览器时，cookie 会在客户端清除，并在“应用程序设置 > 会话持续时间（分钟）” / `session_expire_delay`（默认为 `10080` 分钟 = 7 天）无活动后过期。

当您登录到主应用程序时，您还可以选中 **记住我** 选项。通过 [`devise`](https://github.com/heartcombo/devise) 设置了 `remember_user_token` cookie。`remember_user_token` cookie 在 `config/initializers/devise.rb` -> `config.remember_for` 后过期。默认为 2 周。

当 `_gitlab_session` 过期或不可用时，系统使用 `remember_user_token` 为您获取一个新的 `_gitlab_session`，并通过浏览器重新启动让你保持登录状态。

在您的 `remember_user_token` 过期并且您的 `_gitlab_session` 被清除/过期后，出于安全原因，您将被要求再次登录以验证您的身份。

NOTE:
当任何会话被注销，或者当一个会话通过 [Active Sessions](active_sessions.md) 被撤销时，所有 **记住我** 令牌都会被撤销。当其他会话保持活动状态时，如果浏览器关闭或现有会话过期，**记住我** 功能不会恢复会话。

### 增加登录时间

> 引入于 13.1 版本。

Cookie 的 `remember_user_token` 生命周期现在可以超过 `config.remember_for` 设置的截止日期，因为 `config.extend_remember_period` 标志现在设置为 true。

系统使用会话和持久性 cookie：

- 会话 cookie：当浏览器关闭时，会话 cookie 通常会在浏览器会话结束时删除。`_gitlab_session` cookie 没有固定的到期日期。但是，它会根据其 [`session_expire_delay`](#为什么我总是被注销) 过期。
- 持久性 cookie：`remember_user_token` 是一个有效期为两周的 cookie。如果您在登录时选择 **记住我**，系统会激活此 cookie。

默认情况下，服务器将使用的任何会话的生存时间 (TTL) 设置为 1 周。

当您关闭浏览器时，会话 cookie 可能仍会保留。例如，Chrome 具有恢复会话 cookie 的“从上次中断的地方继续”选项。即只要您至少每 2 周访问一次极狐GitLab，只要您的浏览器选项卡处于打开状态，您就可以保持登录状态。服务器继续重置该会话的 TTL，与是否安装 2FA 无关，如果您关闭浏览器并再次打开它，`remember_user_token` cookie 允许您的用户重新进行身份验证。

如果没有 `config.extend_remember_period` 标志，您将在两周后被迫再次登录。

<!--
## Related topics

- [Create users](account/create_accounts.md)
- [Sign in to your GitLab account](../../topics/authentication/index.md)
- [Receive emails for sign-ins from unknown IP addresses or devices](unknown_sign_in_notification.md)
- Manage applications that can [use GitLab as an OAuth provider](../../integration/oauth_provider.md#introduction-to-oauth)
- Manage [personal access tokens](personal_access_tokens.md) to access your account via API and authorized applications
- Manage [SSH keys](../../ssh/index.md) to access your account via SSH
- Change your [syntax highlighting theme](preferences.md#syntax-highlighting-theme)
- [View your active sessions](active_sessions.md) and revoke any of them if necessary
-->