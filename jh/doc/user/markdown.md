---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# GitLab Flavored Markdown **(FREE)**

极狐GitLab 自动渲染 Markdown 内容。例如，当您向议题添加评论时，您使用 Markdown 语言键入文本。保存议题时，文本会使用一组样式渲染。此页面描述了这些样式。

例如，在 Markdown 中，无序列表如下所示：

```markdown
- Cat
- Dog
- Turtle
```

渲染此列表后，如下所示：

- Cat
- Dog
- Turtle

这些样式**仅适用于极狐GitLab**。[极狐GitLab 文档中心](https://docs.gitlab.cn)和[极狐GitLab 官方网站](https://about.gitlab.cn) 则使用 [Kramdown](https://kramdown.gettalong.org)。

您不应在文档中心查看此页面，应在[极狐GitLab 上查看这些样式如何呈现](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md)。

GitLab Flavored Markdown 扩展了 [CommonMark 规范](https://spec.commonmark.org/current/)。
它的灵感来自 [GitHub Flavored Markdown](https://docs.github.com/en/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)。

## 可以在何处使用

您可以在以下领域使用 GitLab Flavored Markdown：

- 评论
- 议题
- 合并请求
- 里程碑
- 代码片段（片段必须以 `.md` 扩展名命名）
- Wiki 页面
- 仓库中的 Markdown 文档
- 史诗

您还可以在极狐GitLab 中使用其他富文本文件。您可能必须安装依赖项才能执行此操作。<!--For more information, see the [`gitlab-markup` gem project](https://gitlab.com/gitlab-org/gitlab-markup).-->

### GitLab Flavored Markdown 和标准 Markdown 的不同

极狐GitLab 使用标准的 CommonMark 格式。然而，GitLab Flavored Markdown 扩展了标准 Markdown 的特性，专门为极狐GitLab 设计。

标准 Markdown 中没有的功能：

- [以 `HEX`、`RGB` 或 `HSL` 编写的色卡](#颜色)
- [图表和流程图](#图表和流程图)
- [Emoji](#emojis)
- [Front matter](#front-matter)
- [内联差异](#内联差异)
- [用 LaTeX 编写的数学方程和符号](#math)
- [任务列表](#任务列表)
- [目录](#目录)
- [Wiki 特定的 Markdown](#wiki-specific-markdown)

[从标准 Markdown 扩展](#从标准-markdown-扩展的功能)的功能：

| 标准 Markdown                     | 极狐GitLab 的扩展 Markdown 功能 |
| ------------------------------------- | ------------------------- |
| [块引用](#块引用)           | [多行块引用](#多行块引用) |
| [代码块](#code-spans-and-blocks) | [彩色代码和语法高亮](#彩色代码和语法高亮) |
| [强调](#强调)                 | [单词中的多个下划线](#单词中的多个下划线和单词中的强调)
| [headers](#headers)                   | [可链接的 Header IDs](#header-id-和链接) |
| [图片](#图片)                     | [嵌入式视频](#视频)和[音频](音频) |
| [换行](#换行)           | [更多换行控制](#换行符) |
| [链接](#链接)                       | [自动链接 URL](#网址自动链接) |

## 标准 Markdown 中没有的功能

以下功能在标准 Markdown 中找不到。

### 颜色

[在极狐GitLab 中查看此主题](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#颜色)。

您可以使用以下格式编写颜色：`HEX`、`RGB` 或 `HSL`。

- `HEX`: `` `#RGB[A]` `` or `` `#RRGGBB[AA]` ``
- `RGB`: `` `RGB[A](R, G, B[, A])` ``
- `HSL`: `` `HSL[A](H, S, L[, A])` ``

不支持命名颜色。

反引号中的颜色后跟颜色指示符：

```markdown
- `#F00`
- `#F00A`
- `#FF0000`
- `#FF0000AA`
- `RGB(0,255,0)`
- `RGB(0%,100%,0%)`
- `RGBA(0,255,0,0.3)`
- `HSL(540,70%,50%)`
- `HSLA(540,70%,50%,0.3)`
```

- `#F00`
- `#F00A`
- `#FF0000`
- `#FF0000AA`
- `RGB(0,255,0)`
- `RGB(0%,100%,0%)`
- `RGBA(0,255,0,0.3)`
- `HSL(540,70%,50%)`
- `HSLA(540,70%,50%,0.3)`

### 图表和流程图

您可以使用 [Mermaid](https://mermaidjs.github.io/) 或 [PlantUML](https://plantuml.com) 从文本生成图表和流程图。
您还可以使用 [Kroki](https://kroki.io) 来创建各种各样的图表。

#### Mermaid

访问[官方页面](https://mermaidjs.github.io/)了解更多详情。[Mermaid 实时编辑器](https://mermaid-js.github.io/mermaid-live-editor/) 帮助您学习 Mermaid 并 debug Mermaid 代码中的问题，使用它来识别和解决图表中的问题。

要生成图表或流程图，请在 `mermaid` 块中写入文本：

````markdown
```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
````

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

您还可以包含子图：

````markdown
```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```
````


```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```

<!--
#### PlantUML

To make PlantUML available in GitLab, a GitLab administrator must enable it. For more information, see the
[PlantUML & GitLab](../administration/integration/plantuml.md) page.

#### Kroki

To make Kroki available in GitLab, a GitLab administrator must enable it.
For more information, see the [Kroki integration](../administration/integration/kroki.md) page.
-->

### Emojis

[在极狐GitLab 中查看此主题](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#emojis)。


```markdown
Sometimes you want to :monkey: around a bit and add some :star2: to your :speech_balloon:. Well we have a gift for you:

:zap: You can use emoji anywhere GitLab Flavored Markdown is supported. :v:

You can use it to point out a :bug: or warn about :speak_no_evil: patches. And if someone improves your really :snail: code, send them some :birthday:. People :heart: you for that.

If you're new to this, don't be :fearful:. You can join the emoji :family:. Just look up one of the supported codes.

Consult the [Emoji Cheat Sheet](https://www.emojicopy.com) for a list of all supported emoji codes. :thumbsup:
```

Sometimes you want to <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/monkey.png" width="20px" height="20px" style="display:inline;margin:0;border: 0"> around a bit and add some <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/star2.png" width="20px" height="20px" style="display:inline;margin:0;border: 0"> to your <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/speech_balloon.png" width="20px" height="20px" style="display:inline;margin:0;border: 0">. Well we have a gift for you:

<img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/zap.png" width="20px" height="20px" style="display:inline;margin:0;border: 0">You can use emoji anywhere GitLab Flavored Markdown is supported. <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/v.png" width="20px" height="20px" style="display:inline;margin:0;border: 0">

You can use it to point out a<img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/bug.png" width="20px" height="20px" style="display:inline;margin:0;border: 0"> or warn about <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/speak_no_evil.png" width="20px" height="20px" style="display:inline;margin:0;border: 0"> patches. If someone improves your really <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/snail.png" width="20px" height="20px" style="display:inline;margin:0;border: 0"> code, send them some <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/birthday.png" width="20px" height="20px" style="display:inline;margin:0;border: 0">. People <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/heart.png" width="20px" height="20px" style="display:inline;margin:0;border: 0"> you for that.

If you're new to this, don't be <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/fearful.png" width="20px" height="20px" style="display:inline;margin:0;border: 0">. You can join the emoji <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/family.png" width="20px" height="20px" style="display:inline;margin:0;border: 0">. Just look up one of the supported codes.

Consult the [Emoji Cheat Sheet](https://www.webfx.com/tools/emoji-cheat-sheet/) for a list of all supported emoji codes. <img src="https://gitlab.com/gitlab-org/gitlab-foss/raw/master/public/-/emojis/2/thumbsup.png" width="20px" height="20px" style="display:inline;margin:0;border: 0">

#### Emojis

前面的表情符号示例使用硬编码图像。极狐GitLab 中呈现的表情符号可能会因使用的操作系统和浏览器而异。

大多数表情符号在 macOS、Windows、iOS、Android 上都得到了本机支持，并且在不支持的情况下回退到基于图像的表情符号。

<!-- vale gitlab.Spelling = NO -->

在 Linux 上，您可以下载 [Noto Color Emoji](https://github.com/googlefonts/noto-emoji) 以获得完整的原生表情符号支持。Ubuntu 18.04（像许多现代 Linux 发行版一样）默认安装了这种字体。

<!-- vale gitlab.Spelling = YES -->

### Front matter

Front Matter 是包含在 Markdown 文档开头、内容之前的元数据。这些数据可以被静态站点生成器使用，如 [Jekyll](https://jekyllrb.com/docs/front-matter/)、[Hugo](https://gohugo.io/content-management/front-matter/ )，以及许多其他应用程序。

当您查看由极狐GitLab 渲染的 Markdown 文件时，Front Matter 内容按原样显示在文档顶部的框中。HTML 内容显示在前面的内容之后。要查看示例，您可以在[极狐GitLab 文档文件](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/index.md) 的源版本和渲染版本之间切换。

在极狐GitLab 中，Front Matter 仅用于 Markdown 文件和 wiki 页面，其他支持 Markdown 格式的地方不使用。它必须位于文档的最顶部，并且必须位于分隔符之间。

支持以下分隔符：

- YAML (`---`):

  ```yaml
  ---
  title: About Front Matter
  example:
  language: yaml
  ---
  ```

- TOML (`+++`):

  ```toml
  +++
  title = "About Front Matter"
  [example]
  language = "toml"
  +++
  ```

- JSON (`;;;`):

  ```json
  ;;;
  {
    "title": "About Front Matter"
    "example": {
      "language": "json"
    }
  }
  ;;;
  ```

通过向任何现有分隔符添加说明符来支持其他语言。例如：

```php
---php
$title = "About Front Matter";
$example = array(
  'language' => "php",
);
---
```

### 内联差异

[在极狐GitLab 中查看此主题](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#内联差异)。

使用内联差异标签，您可以显示 `{+ additions +}` 或 `[- deletions -]`。

环绕标签可以是花括号或方括号：

```markdown
- {+ addition 1 +}
- [+ addition 2 +]
- {- deletion 3 -}
- [- deletion 4 -]
```

![Inline diff as rendered by the GitLab interface](img/inline_diff_01_v13_3.png)

---

但是，您不能混合标签：

```markdown
- {+ addition +]
- [+ addition +}
- {- deletion -]
- [- deletion -}
```

如果您的差异包含 `` `code` `` 字体中的单词，请确保使用反斜杠 `\` 转义每个反引号 `` ` ``。否则差异突出显示无法正确渲染：

```markdown
- {+ Just regular text +}
- {+ Text with `backticks` inside +}
- {+ Text with escaped \`backticks\` inside +}
```

![Inline diff with mixed formatting, as rendered by the GitLab interface](img/inline_diff_02_v13_3.png)

### Math

[在极狐GitLab 中查看此主题](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#math).

用 LaTeX 语法编写的 Math 用 [KaTeX](https://github.com/KaTeX/KaTeX) 渲染。

在符号 `$` 之间书写的 Math 与文本内联渲染。在[代码块](#code-spans-and-blocks)中编写的 Math 语言声明为 `math`，在单独的行中呈现：

````markdown
This math is inline: $`a^2+b^2=c^2`$.

This math is on a separate line:

```math
a^2+b^2=c^2
```
````

This math is inline: $`a^2+b^2=c^2`$.

This math is on a separate line:

```math
a^2+b^2=c^2
```

*KaTeX 仅支持 LaTeX 的 [子集](https://katex.org/docs/supported.html)。*

这个语法也适用于 Asciidoctor `:stem: latexmath`。有关详细信息，请参阅 [Asciidoctor 用户手册](https://asciidoctor.org/docs/user-manual/#activation-stem-support)。

### 任务列表

[在极狐GitLab 中查看此主题](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#任务列表)。

您可以在任何支持 Markdown 的地方添加任务列表。

- 在议题、合并请求和评论中，您可以单击选择框。
- 在所有其他地方，您不能单击选择框。您必须通过在括号中添加或删除 `x` 来手动编辑 Markdown。

要创建任务列表，请遵循有序或无序列表的格式：

```markdown
- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3

1. [x] Completed task
1. [ ] Incomplete task
   1. [ ] Sub-task 1
   1. [x] Sub-task 2
```

![Task list as rendered by GitLab](img/completed_tasks_v13_3.png)

### 目录

目录是链接到文档中副标题的无序列表。
您可以向议题和合并请求添加目录，但不能向注释或评论添加目录。将 `[[_TOC_]]` 或 `[TOC]` 标签单独添加到任何支持的内容类型的 **Description** 字段：

- Markdown 文件。
- Wiki 页面。
- 议题。
- 合并请求。

```markdown
This sentence introduces my wiki page.

[[_TOC_]]

## My first heading

First section content.

## My second heading

Second section content.
```

![Preview of an auto-generated table of contents in a Wiki](img/markdown_toc_preview_v12_9.png)

### Wiki-specific Markdown

以下主题展示了 wiki 内链接的行为方式。

#### Wiki - 直接页面链接

直接页面链接包括指向该页面的页面 slug，位于 wiki 的基本级别。

此示例链接到 wiki 根目录下的 `documentation` 页面：

```markdown
[Link to Documentation](documentation)
```

#### Wiki - 直接文件链接

直接文件链接指向文件的文件扩展名，相对于当前页面。

如果以下示例位于 `<your_wiki>/documentation/related` 的页面上，它会链接到 `<your_wiki>/documentation/file.md`：

```markdown
[Link to File](file.md)
```

#### Wiki - 层次链接

可以使用 `./<page>`、`../<page>` 等构建相对于当前 wiki 页面的分层链接。

如果此示例位于 `<your_wiki>/documentation/main` 的页面上，它会链接到 `<your_wiki>/documentation/related`：

```markdown
[Link to Related Page](related)
```

如果此示例位于 `<your_wiki>/documentation/related/content` 的页面上，它会链接到 `<your_wiki>/documentation/main`：

```markdown
[Link to Related Page](../main)
```

如果此示例位于 `<your_wiki>/documentation/main` 的页面上，它会链接到 `<your_wiki>/documentation/related.md`：

```markdown
[Link to Related Page](related.md)
```

如果此示例位于 `<your_wiki>/documentation/related/content` 的页面上，它会链接到 `<your_wiki>/documentation/main.md`：

```markdown
[Link to Related Page](../main.md)
```

#### Wiki - 根链接

根链接以 `/` 开头，并且相对于 wiki 根路径。

这个例子链接到 `<wiki_root>/documentation`：

```markdown
[Link to Related Page](/documentation)
```

这个例子链接到 `<wiki_root>/miscellaneous.md`：

```markdown
[Link to Related Page](/miscellaneous.md)
```

## 极狐GitLab 特定的引用

GitLab Flavored Markdown 渲染极狐GitLab 特定的引用。例如，您可以引用一个议题、一个提交、一个团队成员，甚至整个项目团队。GitLab Flavored Markdown 将该引用转换为链接，以便您可以在它们之间导航。所有对项目的引用都应使用**项目 slug** 而不是项目名称。

此外，GitLab Flavored Markdown 识别某些交叉项目引用，并且还有一个简单版本来引用来自同一命名空间的其他项目。

GitLab Flavored Markdown 可以识别：

| 引用                                           | 输入                         | 跨项目引用                    | 同一命名空间内的快捷方式   |
| :--------------------------------------------------- | :---------------------------- | :----------------------------------------- | :------------------------------- |
| 特定用户                                        | `@user_name`                  |                                            |                                  |
| 特定群组                                       | `@group_name`                 |                                            |                                  |
| 整个团队                                          | `@all`                        |                                            |                                  |
| 项目                                              | `namespace/project>`          |                                            |                                  |
| 议题                                               | ``#123``                      | `namespace/project#123`                    | `project#123`                    |
| 合并请求                                      | `!123`                        | `namespace/project!123`                    | `project!123`                    |
| 代码片段                                            | `$123`                        | `namespace/project$123`                    | `project$123`                    |
| [史诗](group/epics/index.md)                         | `&123`                        | `group1/subgroup&123`                      |                                  |
| 漏洞 **(ULTIMATE)** <sup>1</sup>            | `[vulnerability:123]`         | `[vulnerability:namespace/project/123]`    | `[vulnerability:project/123]`    |
| 功能标志                                         | `[feature_flag:123]`          | `[feature_flag:namespace/project/123]`     | `[feature_flag:project/123]`     |
| 标记 ID                                          | `~123`                        | `namespace/project~123`                    | `project~123`                    |
| 单个词的标记名称                              | `~bug`                        | `namespace/project~bug`                    | `project~bug`                    |
| 多个词的标记名称                            | `~"feature request"`          | `namespace/project~"feature request"`      | `project~"feature request"`      |
| 范围标记名称                                 | `~"priority::high"`           | `namespace/project~"priority::high"`       | `project~"priority::high"`       |
| 项目里程碑 ID                              | `%123`                        | `namespace/project%123`                    | `project%123`                    |
| 单个词的里程碑名称                           | `%v1.23`                      | `namespace/project%v1.23`                  | `project%v1.23`                  |
| 多个词的里程碑名称                         | `%"release candidate"`        | `namespace/project%"release candidate"`    | `project%"release candidate"`    |
| 特定提交                                      | `9ba12248`                    | `namespace/project@9ba12248`               | `project@9ba12248`               |
| 提交范围比较                              | `9ba12248...b19a04f5`         | `namespace/project@9ba12248...b19a04f5`    | `project@9ba12248...b19a04f5`    |
| 仓库文件引用                           | `[README](doc/README.md)`     |                                            |                                  |
| 仓库文件行引用                     | `[README](doc/README.md#L13)` |                                            |                                  |
| 警报<!--[警报](../operations/incident_management/alerts.md)--> | `^alert#123`                  | `namespace/project^alert#123`              | `project^alert#123`              |

1. 引入于 13.7 版本。

例如，通过使用`#123` 引用一个议题，将输出格式化为带有文本 `#123` 的议题编号 123 的链接。同样，议题编号 123 的链接被识别并格式化为文本 `#123`。如果您不希望 `#123` 链接到某个议题，请添加前导反斜杠 `\#123`。

除此之外，一些对象的链接也被识别和格式化。示例：

- 议题评论：`"https://gitlab.com/gitlab-org/gitlab/-/issues/1234#note_101075757"`，渲染为 `#1234 (comment 101075757)`
- 议题设计选项卡：`"https://gitlab.com/gitlab-org/gitlab/-/issues/1234/designs"`，渲染为 `#1234 (designs)`.
- 个人设计的链接：`"https://gitlab.com/gitlab-org/gitlab/-/issues/1234/designs/layout.png"`，渲染为 `#1234[layout.png]`.

### 在引用中显示议题、合并请求或史诗标题

> 引入于 14.6 版本。

要在议题、合并请求或史诗的展示链接中包含标题，请在引用末尾添加加号 (`+`)。例如，像 `#123+` 这样的引用展示为 `The issue title (#123)`。

像 `https://gitlab.com/gitlab-org/gitlab/-/issues/1234+` 这样的 URL 引用也会被扩展。

<!--
### Embedding metrics in GitLab Flavored Markdown

Metric charts can be embedded in GitLab Flavored Markdown. Read
[Embedding Metrics in GitLab flavored Markdown](../operations/metrics/embed.md) for more details.
-->

## 从标准 Markdown 扩展的功能

所有标准 Markdown 格式都应该在极狐GitLab 中按预期工作。在不影响标准使用的情况下，一些标准功能通过附加功能进行了扩展。
如果扩展了某个功能，则新选项将作为子部分列出。

### 块引用

使用块引用来突出信息，例如旁注。它是通过以 `>` 开头的块引用行生成的：

```markdown
> Blockquotes help you emulate reply text.
> This line is part of the same quote.

Quote break.

> This very long line is still quoted properly when it wraps. Keep writing to make sure this line is long enough to actually wrap for everyone. You can also *add* **Markdown** into a blockquote.
```

> Blockquotes help you emulate reply text.
> This line is part of the same quote.

Quote break.

> This very long line is still quoted properly when it wraps. Keep writing to make sure this line is long enough to actually wrap for everyone. You can also *add* **Markdown** into a blockquote.

#### 多行块引用

如果此部分未正确显示，请[在极狐GitLab 中查看](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#多行块引用)。

GitLab Flavored Markdown 扩展了标准 Markdown，还支持由 `>>>` 围起来的多行块引用：

```markdown
>>>
If you paste a message from somewhere else

that spans multiple lines,

you can quote that without having to manually prepend `>` to every line!
>>>
```

>>>
If you paste a message from somewhere else

that spans multiple lines,

you can quote that without having to manually prepend `>` to every line!
>>>

### Code spans and blocks

您可以突出显示应视为代码而非标准文本的任何内容。

行内代码用单个反引号 `` ` `` 突出显示：

```markdown
Inline `code` has `back-ticks around` it.
```

Inline `code` has `back-ticks around` it.

---

要对较大的代码示例实现类似的效果，您可以：

- 用三重反引号（```` ``` ````）将整个代码块围起来。
- 用三重波浪号 (`~~~`) 将整个代码块围起来。
- 缩进四个或更多空格。

````markdown
```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python code"
    print s
```

    Using 4 spaces
    is like using
    3-backtick fences.
````

```plaintext
~~~
Tildes are OK too.
~~~
```

以上三个示例渲染为：

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python code"
    print s
```

```plaintext
Using 4 spaces
is like using
3-backtick fences.
```

```plaintext
Tildes are OK too.
```

#### 彩色代码和语法高亮

如果此部分未正确展示，[在极狐GitLab 中查看](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#彩色代码和语法高亮)。

极狐GitLab 使用 [Rouge Ruby 库](http://rouge.jneen.net/)，在代码块中进行更丰富多彩的语法高亮显示。有关支持的语言列表，请访问 [Rouge 项目 wiki](https://github.com/rouge-ruby/rouge/wiki/List-of-supported-languages-and-lexers)。
语法高亮仅在代码块中受支持，因此您不能高亮行内代码。

要对代码块进行围栏和应用语法突出显示，请将代码语言附加到开始代码声明、三个反引号（```` ``` ````）或三个波浪号（`~~~`）：

````markdown
```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

```
No language indicated, so no syntax highlighting.
s = "No highlighting is shown for this line."
But let's throw in a <b>tag</b>.
```
````

以上四个示例渲染为：

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

```plaintext
No language indicated, so no syntax highlighting.
s = "No highlighting is shown for this line."
But let's throw in a <b>tag</b>.
```

### 强调

在 Markdown 中，您可以通过多种方式强调文本。您可以使用斜体、粗体、删除线以及将这些强调样式组合在一起。
删除线不是核心 Markdown 标准的一部分，而是 GitLab Flavored Markdown 的一部分。

例子：

```markdown
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with double **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~
```

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with double **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

#### 单词中的多个下划线和单词中的强调

如果此部分未正确展示，[在极狐GitLab 中查看](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#单词中的多个下划线和单词中的强调)。

避免将单词的一部分使用斜体，尤其是在处理经常带有多个下划线的代码和名称时。
GitLab Flavored Markdown 通过忽略单词中的多个下划线扩展了标准 Markdown，以便更好地展示讨论代码的 Markdown 文档：

```markdown
perform_complicated_task

do_this_and_do_that_and_another_thing

but_emphasis is_desired _here_
```

perform_complicated_task

do_this_and_do_that_and_another_thing

but_emphasis is_desired _here_

---

如果只想强调一个词的一部分，仍然可以用星号来完成：

```markdown
perform*complicated*task

do*this*and*do*that*and*another thing
```

perform*complicated*task

do*this*and*do*that*and*another thing

### 脚注

脚注添加指向在 Markdown 文件末尾呈现的注释的链接。

要制作脚注，您需要一个引用标签和一个单独的行（文件中的任何位置）和注释内容。

不管标签名称如何，参考标签的相对顺序决定了展示的编号。

<!--
Do not edit the following codeblock. It uses HTML to skip the Vale ReferenceLinks test.
-->

<pre class="highlight"><code>A footnote reference tag looks like this: [^1]

This reference tag is a mix of letters and numbers. [^footnote-42]

&#91;^1]: This text is inside a footnote.

&#91;^footnote-42]: This text is another footnote.
</code></pre>

A footnote reference tag looks like this:[^1]

This reference tag is a mix of letters and numbers.[^footnote-42]

<!--
Do not delete the single space before the [^1] and [^footnotes] references below.
These are used to force the Vale ReferenceLinks check to skip these examples.
-->

 [^1]: This text is inside a footnote.

 [^footnote-42]: This text is another footnote.

### Headers

```markdown
# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:

Alt-H1
======

Alt-H2
------
```

#### Header ID 和链接

GitLab Flavored Markdown 扩展了 Markdown 标准，以便所有 Markdown 渲染的 Header 自动获取 ID，可以链接到 ID，注释除外。

在悬停时，指向这些 ID 的链接变得可见，以便更轻松地将链接复制到标题以在其他地方使用它。

根据以下规则从 Header 的内容生成 ID：

1. 所有文本都转换为小写。
1. 删除所有非文字文本（如标点或 HTML）。
1. 所有空格都转换为连字符。
1. 连续两个或多个连字符转换为一个。
1. 如果已经生成了相同 ID 的 header，则附加一个唯一的递增编号，从 1 开始。

示例：

```markdown
# This header has spaces in it
## This header has a :thumbsup: in it
# This header has Unicode in it: 한글
## This header has spaces in it
### This header has spaces in it
## This header has 3.5 in it (and parentheses)
```

将生成以下链接 ID：

1. `this-header-has-spaces-in-it`
1. `this-header-has-a-in-it`
1. `this-header-has-unicode-in-it-한글`
1. `this-header-has-spaces-in-it-1`
1. `this-header-has-spaces-in-it-2`
1. `this-header-has-3-5-in-it-and-parentheses`

表情符号处理发生在标题 ID 生成之前。表情符号被转换为图像，然后从 ID 中删除。

### 水平线

使用三个或更多连字符、星号或下划线创建水平线：

```markdown
Three or more hyphens,

---

asterisks,

***

or underscores

___
```

### 图片

示例：

<!--
Do not edit the following codeblock. It uses HTML to skip the Vale ReferenceLinks test.
-->

<pre class="highlight"><code>Inline-style (hover to see title text):

![alt text](img/markdown_logo.png "Title Text")

Reference-style (hover to see title text):

![alt text1][logo]

&#91;logo]: img/markdown_logo.png "Title Text"
</code></pre>

<!--
DO NOT change the name of markdown_logo.png. This file is used for a test in
spec/controllers/help_controller_spec.rb.
-->

Inline-style (hover to see title text):

![alt text](img/markdown_logo.png "Title Text")

Reference-style (hover to see title text):

<!--
The example below uses an in-line link to pass the Vale ReferenceLinks test.
Do not change to a reference style link.
-->

![alt text](img/markdown_logo.png "Title Text")

在极少数情况下，您必须为图像设置特定的高度或宽度，您可以使用 `img` HTML 标签代替 Markdown 并设置其 `height` 和 `width` 参数。

#### 视频

如果此部分未正确展示，[在极狐GitLab 中查看](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#视频)。

链接到带有视频扩展名的文件的图像标签会自动转换为视频播放器。有效的视频扩展名是 `.mp4`、`.m4v`、`.mov`、`.webm` 和 `.ogv`：

```markdown
Here's a sample video:

![Sample Video](img/markdown_video.mp4)
```

Here's a sample video:

![Sample Video](img/markdown_video.mp4)

#### 音频

如果此部分未正确展示，[在极狐GitLab 中查看](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#音频)。

与视频类似，带有音频扩展名的文件的链接标签会自动转换为音频播放器。有效的音频扩展名为 `.mp3`、`.oga`、`.ogg`、`.spx` 和 `.wav`：

```markdown
Here's a sample audio clip:

![Sample Audio](img/markdown_audio.mp3)
```

Here's a sample audio clip:

![Sample Audio](img/markdown_audio.mp3)

### 行内 HTML

> 允许 `rel="license"` 引入于 14.6 版本。

要查看以 HTML 呈现的第二个 Markdown 示例，[在极狐GitLab 中查看](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#行内-html)。

您还可以在 Markdown 中使用原始 HTML，它通常效果很好。

要查看允许的 HTML 标签和属性列表的类，请参阅 HTML::Pipeline 的 [SanitizationFilter](https://github.com/jch/html-pipeline/blob/v2.12.3/lib/html/pipeline/sanitization_filter.rb#L42) 的文档。除了默认的 `SanitizationFilter` 许可名单，还允许使用 `span`、`abbr`、`details` 和 `summary` 元素。
`rel="license"` 允许用于支持 [Rel-License 微格式](https://microformats.org/wiki/rel-license) 和许可证归属的链接。

```html
<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. HTML <em>tags</em> do <b>work</b>, in most cases.</dd>
</dl>
```

<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. HTML <em>tags</em> do <b>work</b>, in most cases.</dd>
</dl>

---

仍然可以在 HTML 标签中使用 Markdown，但前提是包含 Markdown 的行被分成自己的行：

```html
<dl>
  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. HTML tags work, in most cases.</dd>

  <dt>Markdown in HTML</dt>
  <dd>

  Does *not* work **very** well. HTML tags work, in most cases.

  </dd>
</dl>
```

<!--
The example below uses HTML to force correct rendering on docs.gitlab.com,
Markdown is fine in GitLab.
-->

<dl>
  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. HTML tags work, in most cases.</dd>

  <dt>Markdown in HTML</dt>
  <dd>

  Does <em>not</em> work <b>very</b> well. HTML tags work, in most cases.

  </dd>
</dl>

#### 可折叠部分

要查看以 HTML 呈现的第二个 Markdown 示例，[在极狐GitLab 中查看](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#可折叠部分)。

可以使用 HTML 的 [`<details>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details) 和 [`<summary>`](https:/ /developer.mozilla.org/en-US/docs/Web/HTML/Element/summary) 标签。例如，折叠一个长日志文件以减少它占用的屏幕空间。

```html
<p>
<details>
<summary>Click this to collapse/fold.</summary>

These details <em>remain</em> <strong>hidden</strong> until expanded.

<pre><code>PASTE LOGS HERE</code></pre>

</details>
</p>
```

<p>
<details>
<summary>Click this to collapse/fold.</summary>

These details <em>remain</em> <strong>hidden</strong> until expanded.

<pre><code>PASTE LOGS HERE</code></pre>

</details>
</p>

---

支持这些标签内的 Markdown。

NOTE:
如果您的 Markdown 未正确展示，请尝试将 `{::options parse_block_html="true" /}` 添加到页面顶部，并将 `markdown="span"` 添加到开始摘要标记中，如下所示：`< 摘要 markdown="span">`。

记得在`</summary>` 标签之后和`</details>` 标签之前留一个空行，如示例所示：

````html
<details>
<summary>Click this to collapse/fold.</summary>

These details _remain_ **hidden** until expanded.

```
PASTE LOGS HERE
```

</details>
````

<!--
The example below uses HTML to force correct rendering on docs.gitlab.com, Markdown
works correctly in GitLab.
-->

<details>
<summary>Click this to collapse/fold.</summary>

These details <em>remain</em> <b>hidden</b> until expanded.

<pre><code>PASTE LOGS HERE</code></pre>

</details>

### 换行

如果前一个文本以两个换行符结尾，则插入换行符（新段落开始），例如连续按 <kbd>Enter</kbd> 两次。如果您只使用一个换行符（选择 <kbd>Enter</kbd> 一次），下一个句子仍然是同一段落的一部分。如果您想防止长行换行并保持它们可编辑，请使用此方法：

```markdown
Here's a line for us to start with.

This longer line is separated from the one above by two newlines, so it is a *separate paragraph*.

This line is also a separate paragraph, but...
These lines are only separated by single newlines,
so they *do not break* and just follow the previous lines
in the *same paragraph*.
```

Here's a line for us to start with.

This longer line is separated from the one above by two newlines, so it is a *separate paragraph*.

This line is also a separate paragraph, but...
These lines are only separated by single newlines,
so they *do not break* and just follow the previous lines
in the *same paragraph*.

#### 换行符

GitLab Flavored Markdown 遵循 Markdown 规范来处理[段落和换行符](https://spec.commonmark.org/current/)。

一个段落是一个或多个连续的文本行，由一个或多个空行（第一段末尾的两个换行符）分隔，如[上面解释](#换行)。

要更多地控制换行符或软返回，通过以反斜杠或两个或多个空格结束一行来添加单个换行符。一行中的两个换行符创建一个新段落，中间有一个空行：

```markdown
First paragraph.
Another line in the same paragraph.
A third line in the same paragraph, but this time ending with two spaces.{space}{space}
A new line directly under the first paragraph.

Second paragraph.
Another line, this time ending with a backslash.\
A new line due to the previous backslash.
```

### 链接

您可以通过两种方式创建链接：内联样式和引用样式。例如：

<!--
Do not edit the following codeblock. It uses HTML to skip the Vale ReferenceLinks test.
-->

<pre class="highlight"><code>- This line shows an [inline-style link](https://www.google.com)
- This line shows a [link to a repository file in the same directory](index.md)
- This line shows a [relative link to a readme one directory higher](../index.md)
- This line shows a [link that also has title text](https://www.google.com "This link takes you to Google!")

Using header ID anchors:

- This line links to [a section on a different Markdown page, using a "#" and the header ID](index.md#overview)
- This line links to [a different section on the same page, using a "#" and the header ID](#header-ids-and-links)

Using references:

- This line shows a [reference-style link, see below][Arbitrary case-insensitive reference text]
- You can [use numbers for reference-style link definitions, see below][1]
- Or leave it empty and use the [link text itself][], see below.

Some text to show that the reference links can follow later.

&#91;arbitrary case-insensitive reference text]: https://www.mozilla.org/en-US/
&#91;1]: https://slashdot.org
&#91;link text itself]: https://www.reddit.com
</code></pre>

- This line shows an [inline-style link](https://www.google.com)
- This line shows a [link to a repository file in the same directory](index.md)
- This line shows a [relative link to a README one directory higher](../index.md)
- This line shows a [link that also has title text](https://www.google.com "This link takes you to Google!")

Using header ID anchors:

- This line links to [a section on a different Markdown page, using a "#" and the header ID](index.md#overview)
- This line links to [a different section on the same page, using a "#" and the header ID](#header-ids-and-links)

Using references:

<!--
The example below uses in-line links to pass the Vale ReferenceLinks test.
Do not change to reference style links.
-->

- This line is a [reference-style link, see below](https://www.mozilla.org/en-US/)
- You can [use numbers for reference-style link definitions, see below](https://slashdot.org)
- Or leave it empty and use the [link text itself](https://www.reddit.com), see below.

Some text to show that the reference links can follow later.

NOTE:
相对链接不允许引用 wiki 页面中的项目文件，或项目文件中的 wiki 页面。原因：wiki 始终位于极狐GitLab 中的单独 Git 仓库中。 例如，`[I'm a reference-style link](style)` 仅当链接位于 wiki Markdown 文件内时才将链接指向 `wikis/style`。

#### 网址自动链接

GitLab Flavored Markdown 几乎可以自动链接您放入文本中的任何 URL：

```markdown
- https://www.baidu.com
- https://www.baidu.com
- ftp://ftp.us.debian.org/debian/
- smb://foo/bar/baz
- irc://irc.freenode.net/
- http://localhost:3000
```

<!-- vale gitlab.Spelling = NO -->

- <https://www.baidu.com>
- <https://www.baidu.com>
- <ftp://ftp.us.debian.org/debian/>
- <smb://foo/bar/baz>
- <irc://irc.freenode.net/>
- <http://localhost:3000>

<!-- vale gitlab.Spelling = YES -->
### 列表

您可以创建有序和无序列表。

对于有序列表，在有序列表的每一行的开头添加您希望列表开头的数字，例如 `1.`，后跟一个空格。
在第一个数字之后，您使用哪个数字并不重要。有序列表按垂直顺序自动编号，因此对同一列表中的所有项目重复 `1.` 是很常见的。如果从一个不是 `1.` 的数字开始，会使用它作为第一个数字，并从那里开始计数。

例子：

```markdown
1. First ordered list item
2. Another item
   - Unordered sub-list.
1. Actual numbers don't matter, just that it's a number
   1. Ordered sub-list
   1. Next ordered sub-list item
4. And another item.
```

<!--
The "2." and "4." in the example above are changed to "1." below, to match the style
standards on docs.gitlab.com.
See https://docs.gitlab.com/ee/development/documentation/styleguide/index.html#lists
-->

1. First ordered list item
1. Another item
   - Unordered sub-list.
1. Actual numbers don't matter, just that it's a number
   1. Ordered sub-list
   1. Next ordered sub-list item
1. And another item.

对于无序列表，在无序列表的每行开头添加一个 `-`、`*` 或 `+`，后跟一个空格，但不应混合使用它们。

```markdown
Unordered lists can:

- use
- minuses

They can also:

* use
* asterisks

They can even:

+ use
+ pluses
```

<!--
The "*" and "+" in the example above are changed to "-" below, to match the style
standards on docs.gitlab.com.
See https://docs.gitlab.com/ee/development/documentation/styleguide/index.html#lists
-->

Unordered lists can:

- use
- minuses

They can also:

- use
- asterisks

They can even:

- use
- pluses

---

如果列表项包含多个段落，则每个后续段落都应缩进与列表项文本的开头相同的级别。

例子：

```markdown
1. First ordered list item

   Second paragraph of first item.

1. Another item
```

1. First ordered list item

   Second paragraph of first item.

1. Another item

---

如果第一项的段落没有使用适当数量的空格缩进，则该段落将出现在列表之外，而不是在列表项下正确缩进。
例如：

```markdown
1. First ordered list item

  Paragraph of first item.

1. Another item
```

1. First ordered list item

  Paragraph of first item.

1. Another item

### 上标/下标

CommonMark 和 GitLab Flavored Markdown 不支持 Redcarpet 上标语法 (`x^2`)。
对上标和下标使用标准 HTML 语法：

```html
The formula for water is H<sub>2</sub>O
while the equation for the theory of relativity is E = mc<sup>2</sup>.
```

<!-- vale gitlab.Spelling = NO -->

水的公式是H<sub>2</sub>O，而相对论的公式是E = mc<sup>2</sup>。

<!-- vale gitlab.Spelling = YES -->

### 表格

表格不是核心 Markdown 规范的一部分，但它们是 GitLab Flavored Markdown 的一部分。

1. 第一行包含标题，以 “管道”(`|`) 分隔。
1. 第二行将标题与单元格分开。
   - 单元格只能包含空格、连字符和（可选）用于水平对齐的冒号。
   - 每个单元格必须至少包含一个连字符，但向单元格添加更多连字符不会更改单元格的渲染。
   - 不允许出现除连字符、空格或冒号以外的任何内容
1. 第三行和任何后续行包含单元格值。
   - **不能**在 Markdown 中将单元格分隔成多行，它们必须保持为单行，但它们可以很长。如果需要，您还可以包含 HTML `<br>` 标签以强制换行。
   - 单元格大小**不必**必须相互匹配。它们是灵活的，但必须用管道 (`|`) 分隔。
   - **可以**有空白单元格。
1. 列宽根据单元格内容动态计算。

例子：
```markdown
| header 1 | header 2 | header 3 |
| ---      | ---      | ---      |
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It eventually wraps the text when the cell is too large for the display size. |
| cell 7   |          | cell 9   |
```

| header 1 | header 2 | header 3 |
| ---      | ---      | ---      |
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It eventually wraps the text when the cell is too large for the display size. |
| cell 7   |          | cell 9   |

此外，您可以通过在第二行中的“破折号”线的两侧添加冒号 (`:`) 来选择列中文本的对齐方式。这会影响列中的每个单元格：

```markdown
| Left Aligned | Centered | Right Aligned |
| :---         | :---:    | ---:          |
| Cell 1       | Cell 2   | Cell 3        |
| Cell 4       | Cell 5   | Cell 6        |
```

| Left Aligned | Centered | Right Aligned |
| :---         | :---:    | ---:          |
| Cell 1       | Cell 2   | Cell 3        |
| Cell 4       | Cell 5   | Cell 6        |

[在极狐GitLab 本身](https://gitlab.com/gitlab-jh/gitlab/-/blob/master/jh/doc/user/markdown.md#表格)，标题在 Chrome 和 Firefox 中总是左对齐，并且在 Safari 为中心对齐。

您可以使用 HTML 格式来调整表格的展示。例如，您可以使用 `<br>` 标签强制一个单元格具有多行：

```markdown
| Name | Details |
| ---  | ---     |
| Item1 | This text is on one line |
| Item2 | This item has:<br>- Multiple items<br>- That we want listed separately |
```

| Name | Details |
| ---  | ---     |
| Item1 | This text is on one line |
| Item2 | This item has:<br>- Multiple items<br>- That we want listed separately |

您可以在极狐GitLab 本身中使用 HTML 格式来添加带有复选框的[任务列表](#任务列表)<!--，但它们在 `docs.gitlab.com` 上无法正确呈现-->：

```markdown
| header 1 | header 2 |
| ---      | ---      |
| cell 1   | cell 2   |
| cell 3   | <ul><li> - [ ] Task one </li><li> - [ ] Task two </li></ul> |
```

#### 从电子表格复制并粘贴到 Markdown

<!--
[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/27205) in GitLab 12.7.
-->

如果您正在使用电子表格软件（例如，Microsoft Excel、Google 表格或 Apple Numbers），当您从电子表格中复制和粘贴时，系统会创建一个 Markdown 表格。例如，假设您有以下电子表格：

![Copy from spreadsheet](img/markdown_copy_from_spreadsheet_v12_7.png)

选择单元格并将它们复制到剪贴板。打开 GitLab Markdown 条目并粘贴电子表格：

![Paste to Markdown table](img/markdown_paste_table_v12_7.png)

## 参考

- 本文档大量利用了 [Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)。
- Daring Fireball 的原始 [Markdown 语法指南](https://daringfireball.net/projects/markdown/syntax) 是详细解释标准 Markdown 的极好资源。
- 您可以在 [CommonMark Spec](https://spec.commonmark.org/current/) 中找到 CommonMark 的详细规范。
- [CommonMark Dingus](https://spec.commonmark.org/dingus/) 帮助您测试 CommonMark 语法。

<!--
## Transition from Redcarpet to CommonMark

- In GitLab 11.8, the [Redcarpet Ruby library](https://github.com/vmg/redcarpet)
  was removed. All issues and comments, including those in 11.1 and earlier, are now processed
  by using the [CommonMark Ruby Library](https://github.com/gjtorikian/commonmarker).
- In GitLab 11.3 and later, CommonMark processes wiki pages and Markdown
  files (`*.md`) in repositories.
- In GitLab 11.1 and later, the [CommonMark Ruby Library](https://github.com/gjtorikian/commonmarker)
  for Markdown processes all new issues, merge requests, comments, and other Markdown
  content.

The documentation website migrated its Markdown engine
[from Redcarpet to Kramdown](https://gitlab.com/gitlab-org/gitlab-docs/-/merge_requests/108)
in October 2018.

You may have older issues, merge requests, or Markdown documents in your
repository that relied upon nuances of the GitLab RedCarpet version
of Markdown. Because CommonMark uses slightly stricter syntax, these documents
may now appear differently after the transition to CommonMark.

For example, numbered lists with nested lists may
render incorrectly:

```markdown
1. Chocolate
  - dark
  - milk
```

To fix this issue, add a space to each nested item. The `-` must be aligned with the first
character of the top list item (`C` in this case):

```markdown
1. Chocolate
   - dark
   - milk
```

1. Chocolate
   - dark
   - milk

We flag any significant differences between Redcarpet and CommonMark Markdown in this document.

If you have many Markdown files, it can be tedious to determine
if they display correctly or not. You can use the
[`diff_redcarpet_cmark`](https://gitlab.com/digitalmoksha/diff_redcarpet_cmark)
tool to generate a list of files and the
differences between how RedCarpet and CommonMark render the files. It indicates
if any changes are needed.

`diff_redcarpet_cmark` is not an officially supported product.
-->