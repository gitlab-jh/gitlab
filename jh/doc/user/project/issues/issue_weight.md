---
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/issue_weight.html'
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 议题权重 **(PREMIUM)**

> 于 13.9 移动到专业版

当您遇到很多议题时，可能很难获得概览。通过加权议题，您可以更好地了解给定议题的时间、价值或复杂性成本。

通过更改下拉菜单中的值，您可以在创建议题期间设置议题的权重。您可以将其设置为 0、1、2 等范围内的非负整数值。（数据库存储一个 4 字节的值，因此上限基本上是无限的。）
您也可以从议题中删除权重。

该值显示在单个议题的右侧边栏上，以及权重图标 (**{weight}**) 旁边的议题页面中。

作为额外的奖励，您可以在里程碑页面上看到所有议题的权重总和。

![issue page](img/issue_weight_v13_11.png)
