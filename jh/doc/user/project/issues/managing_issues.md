---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 管理议题 **(FREE)**

[极狐GitLab 议题](index.md)是在极狐GitLab 中就想法和计划工作进行协作的基本媒介。

## 创建议题

创建议题时，系统会提示您输入议题的字段。
如果您知道要分配给议题的值，则可以使用[快捷操作](../quick_actions.md)输入它们。

您可以在 GitLab 中以多种方式创建问题：

- [从项目](#从项目)
- [从群组](#从群组)
- [从另一个议题](#从另一个议题)
- [从议题看板](#从议题看板)
- [通过发送电子邮件](#通过发送电子邮件)
- 使用带有预填充字段的 URL
- [使用服务台](#使用服务台)

### 从项目

先决条件：

- 您必须至少拥有该项目的 Guest 角色。

要创建议题：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 您可以：

   - 在左侧边栏上，选择 **议题**，然后在右上角选择 **新建议题**。
   - 在顶部栏上，选择加号 (**{plus-square}**)，然后在 **此项目** 下，选择 **新建议题**。

1. 完成[填写字段](#新建议题表单中的字段)。
1. 选择 **创建议题**。

新创建的议题开放。

### 从群组

议题属于项目，但是当您在群组中时，您可以访问和创建属于群组中的项目的议题。

先决条件：

- 您必须至少具有群组中项目的 Guest 角色。

要从群组创建问题：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **议题**。
1. 在右上角，选择 **选择要创建议题的项目**。
1. 选择您要为其创建议题的项目。该按钮现在反映了选定的项目。
1. 选择 **在 `<project name>` 中新建议题**。
1. 完成[填写字段](#新建议题表单中的字段)。
1. 选择 **创建议题**。

新创建的议题开放。

您最近选择的项目将成为您下次访问的默认项目。
如果您主要为同一项目创建议题，这可以为您节省大量时间和点击次数。

### 从另一个议题

> 新议题与原始议题相关联，引入于 14.3 版本。

您可以从现有议题创建新议题，然后可以将这两个议题标记为相关。

先决条件：

- 您必须至少具有群组中项目的 Guest 角色。

To create an issue from another issue:

1. 在现有议题中，选择垂直省略号 (**{ellipsis_v}**)。
1. 选择 **新建议题**。
1. 完成[填写字段](#新建议题表单中的字段)。
   新议题的描述预先填入了 `Related to #123`，其中 `123` 是议题的来源ID。如果您在描述中保留此提及，则这两个议题将变为[相关联](related_issues.md)。
1. 选择 **创建议题**。

新创建的议题开放。

### 从议题看板

您可以从[议题看板](../issue_board.md)创建新议题。

先决条件：

- 您必须至少具有项目的 Guest 角色。

要从项目议题看板创建议题：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 选择 **议题 > 看板**。
1. 在版块列表的顶部，选择 **新建议题** (**{plus-square}**)。
1. 输入议题的标题。
1. 选择 **创建议题**。

要从群组议题看板创建议题：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 选择 **议题 > 看板**。
1. 在版块列表的顶部，选择 **新建议题** (**{plus-square}**)。
1. 输入议题的标题。
1. 在 **项目** 下，选择议题所属的群组中的项目。
1. 选择 **创建议题**。

议题已创建并显示在看板列表中，它共享列表的特性，因此，例如列表的范围限定为标记 `Frontend`，则新议题也具有此标记。

### 通过发送电子邮件

<!--
> Generated email address format changed in GitLab 11.7.
> The older format is still supported, so existing aliases and contacts still work.
-->

您可以在项目的 **议题列表** 页面上发送电子邮件以在项目中创建议题。

先决条件：

- 您的实例必须配置接收电子邮件<!--[接收电子邮件](../../../administration/incoming_email.md)-->。
- 议题列表中必须至少有一个议题。
- 您必须至少拥有该项目的 Guest 角色。

要将议题通过电子邮件发送到项目：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 选择 **议题**。
1. 在页面底部，选择 **通过电子邮件向该项目发送新议题**。
1. 要复制电子邮件地址，请选择 **复制** (**{copy-to-clipboard}**)。
1. 从您的电子邮件客户端，向该地址发送一封电子邮件。主题用作新议题的标题，电子邮件正文成为描述。您可以使用 [Markdown](../../markdown.md) 和 [快速操作](../quick_actions.md)。

成功后，创建了一个新议题，您的用户作为作者。
您可以将此地址保存为电子邮件客户端中的联系人以再次使用。

WARNING:
您看到的电子邮件地址是私人电子邮件地址，专为您生成。**仅保存给自己**，因为任何知道它的人都可以创建议题或合并请求。

要重新生成电子邮件地址：

1. 在议题列表中，选择 **通过电子邮件向该项目发送新议题**。
1. 选择 **重置此令牌**。

### 使用带有预填值的 URL

要直接链接到带有预填充字段的新议题页面，请在 URL 中使用查询字符串参数。您可以在外部 HTML 页面中嵌入 URL 以使用预填充的某些字段创建议题。

| 字段                | URL 参数         | 备注                                                                                                                          |
| -------------------- | --------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| 标题              | `issue[title]`        | 必须是 URL 编码<!--[URL 编码](../../../api/index.md#namespaced-path-encoding)-->。                                                          |
| 议题类型           | `issue[issue_type]`   | `incident` 或 `issue`。                                                                                                  |
| 描述模板 | `issuable_template`   | 不能与 `issue[description]` 同时使用。必须是 URL 编码<!--[URL 编码](../../../api/index.md#namespaced-path-encoding)-->。 |
| 描述          | `issue[description]`  | 不能与 `issuable_template` 同时使用。必须是 URL 编码<!--[URL 编码](../../../api/index.md#namespaced-path-encoding)-->。  |
| 私密         | `issue[confidential]` | 如果为 `true`，则议题被标记为机密。                                                                                |

调整这些示例以形成带有预填充字段的新议题 URL。
在项目中创建议题：

- 带有预填充的标题和描述：

  ```plaintext
  https://gitlab.com/gitlab-org/gitlab/-/issues/new?issue[title]=Whoa%2C%20we%27re%20half-way%20there&issue[description]=Whoa%2C%20livin%27%20in%20a%20URL
  ```

- 使用预填充的标题和描述模板：

  ```plaintext
  https://gitlab.com/gitlab-org/gitlab/-/issues/new?issue[title]=Validate%20new%20concept&issuable_template=Feature%20Proposal%20-%20basic
  ```

- 带有预填充的标题、描述并标记为机密：

  ```plaintext
  https://gitlab.com/gitlab-org/gitlab/-/issues/new?issue[title]=Validate%20new%20concept&issue[description]=Research%20idea&issue[confidential]=true
  ```

### 使用服务台

要提供电子邮件支持，请为您的项目启用[服务台](../service_desk.md)。

现在，当您的客户发送新电子邮件时，可以在适当的项目中创建新议题并从那里跟进。

### 新建议题表单中的字段

<!--
> Adding the new issue to an epic [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/13847) in GitLab 13.1.
-->

创建新议题时，您可以填写以下字段：

- 标题
- 类型：issue（默认）或 incident
- [描述模板](../description_templates.md)：覆盖描述文本框中的任何内容
- 描述：您可以使用 [Markdown](../../markdown.md) 和 [快速操作](../quick_actions.md)
- 使问题具有[机密性](confidential_issues.md)的复选框
- [指派人](#指派人)
- [权重](issue_weight.md)
- [史诗](../../group/epics/index.md)
- [截止日期](due_dates.md)
- [里程碑](../milestones/index.md)
- [标记](../labels.md)

## 编辑议题

您可以编辑议题的标题和描述。

先决条件：

- 您必须至少具有该项目的报告者角色。

要编辑问题：

1. 在标题右侧，选择 **编辑标题和描述** (**{pencil}**)。
1. 编辑可用字段。
1. 选择 **保存修改**。

### 批量编辑项目中的议题

<!--
> - Assigning epic [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210470) in GitLab 13.2.
> - Editing health status [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/218395) in GitLab 13.2.
> - Editing iteration [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/196806) in GitLab 13.9.
-->

当您在一个项目中时，您可以一次编辑多个议题。

先决条件：

- 您必须至少具有该项目的报告者角色。

同时编辑多个议题：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **议题**。
1. 选择 **编辑议题**。屏幕右侧会出现一个侧边栏。
1. 选中您要编辑的每个议题旁边的复选框。
1. 从侧边栏中，编辑可用字段。
1. 选择 **全部更新**。

在项目中批量编辑议题时，您可以编辑以下属性：

- 状态（开放中或已关闭）
- [指派人](#指派人)
- [史诗](../../group/epics/index.md)
- [里程碑](../milestones/index.md)
- [标记](../labels.md)
- [健康状态](#健康状态)
- [通知](../../profile/notifications.md)订阅
- [迭代](../../group/iterations/index.md)

### 批量编辑群组中的议题

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/7249) in GitLab 12.1.
> - Assigning epic [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210470) in GitLab 13.2.
> - Editing health status [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/218395) in GitLab 13.2.
> - Editing iteration [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/196806) in GitLab 13.9.
-->

当您在一个群组中时，您可以跨多个项目编辑多个议题。

先决条件：

- 您必须至少具有该项目的报告者角色。

同时编辑多个议题：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **议题**。
1. 选择 **编辑议题**。屏幕右侧会出现一个侧边栏。
1. 选中您要编辑的每个议题旁边的复选框。
1. 从侧边栏中，编辑可用字段。
1. 选择 **全部更新**。

在群组中批量编辑议题时，您可以编辑以下属性：

- [史诗](../../group/epics/index.md)
- [里程碑](../milestones/index.md)
- [迭代](../../group/iterations/index.md)
- [标记](../labels.md)
- [健康状态](#健康状态)

## 移动议题

当您移动议题时，它会关闭并复制到目标项目。
原始议题没有被删除。系统注释被添加到两个议题中，显示它来源和qu'xiang。 

先决条件：

- 您必须至少具有该项目的报告者角色。

移动问题：

1. 转到议题。
1. 在右侧边栏上，选择 **移动议题**。
1. 搜索将议题移至的项目。
1. 选择 **移动**。

### 批量移动议题 **(FREE SELF)**

您可以将所有未解决的议题从一个项目移动到另一个项目。

先决条件：

- 您必须至少具有该项目的报告者角色。

1. 可选（但推荐），在控制台中尝试任何更改之前创建备份<!--[创建备份](../../../raketasks/backup_restore.md)-->。
1. 打开 Rails 控制台<!--[Rails 控制台](../../../administration/operations/rails_console.md)-->。
1. 运行以下脚本。确保将 `project`、`admin_user` 和 `target_project` 更改为您的值。

   ```ruby
   project = Project.find_by_full_path('full path of the project where issues are moved from')
   issues = project.issues
   admin_user = User.find_by_username('username of admin user') # make sure user has permissions to move the issues
   target_project = Project.find_by_full_path('full path of target project where issues moved to')

   issues.each do |issue|
      if issue.state != "closed" && issue.moved_to.nil?
         Issues::MoveService.new(project, admin_user).execute(issue, target_project)
      else
         puts "issue with id: #{issue.id} and title: #{issue.title} was not moved"
      end
   end; nil
   ```

1. 要退出 Rails 控制台，请输入 `quit`。

## 关闭议题

当您决定某个议题已解决或不再需要时，您可以关闭它。
该议题已标记为已关闭但未删除。

先决条件：

- 您必须至少具有该项目的报告者角色。

要关闭议题，您可以执行以下操作：

- 在议题的顶部，选择 **关闭议题**。
- 在[议题看板](../issue_board.md)中，将议题卡从其列表拖到 **已关闭** 列表中。

  ![close issue from the issue board](img/close_issue_from_board.gif)

### 重新打开关闭的议题

先决条件：

- 您必须至少具有该项目的报告者角色。

要重新打开已关闭的议题，请在议题顶部选择 **重启议题**。
重新打开的问题与任何其他未解决的问题没有不同。

### 自动关闭议题

您可以通过在提交消息或 MR 描述中使用某些词来自动关闭议题。

如果提交消息或合并请求描述包含与[定义的 pattern](#默认关闭-pattern) 匹配的文本，则在以下任一情况下，匹配文本中引用的所有议题都将关闭：

- 提交被推送到项目的[**默认**分支](../repository/branches/default.md)。
- 提交或合并请求，合并到默认分支中。

例如，如果您在合并请求描述中包含 `Closes #4, #6, Related to #5`：

- 合并 MR 时会自动关闭议题 `#4` 和 `#6`。
- 议题 `#5` 被标记为[相关议题](related_issues.md)，但它不会自动关闭。

或者，当您[从议题创建合并请求](../merge_requests/getting_started.md#合并请求关闭议题)时，它会继承议题的里程碑和标签。

出于性能原因，从现有仓库第一次推送时禁用自动关闭议题。

#### 默认关闭 pattern

要自动关闭议题，请使用以下关键字，后跟议题引用。

可用关键字：

- Close, Closes, Closed, Closing, close, closes, closed, closing
- Fix, Fixes, Fixed, Fixing, fix, fixes, fixed, fixing
- Resolve, Resolves, Resolved, Resolving, resolve, resolves, resolved, resolving
- Implement, Implements, Implemented, Implementing, implement, implements, implemented, implementing

可用的议题引用格式：

- 本地议题 (`#123`).
- 跨项目议题 (`group/project#123`).
- 议题的完整 URL (`https://gitlab.example.com/group/project/issues/123`).

示例：

```plaintext
Awesome commit message

Fix #20, Fixes #21 and Closes group/otherproject#22.
This commit is also related to #17 and fixes #18, #19
and https://gitlab.example.com/group/otherproject/issues/23.
```

上一个提交消息关闭了此提交推送到的项目中的`#18`、`#19`、`#20` 和 `#21`，以及 `group/otherproject` 中的 `#22` 和 `#23`。`#17` 没有关闭，因为它与 pattern 不匹配。

您可以在多行提交消息或单行中使用关闭模式，使用 `git commit -m` 从命令行完成。

默认议题结束 pattern 正则表达式：

```shell
\b((?:[Cc]los(?:e[sd]?|ing)|\b[Ff]ix(?:e[sd]|ing)?|\b[Rr]esolv(?:e[sd]?|ing)|\b[Ii]mplement(?:s|ed|ing)?)(:?) +(?:(?:issues? +)?%{issue_ref}(?:(?: *,? +and +| *,? *)?)|([A-Z][A-Z0-9_]+-\d+))+)
```

#### 禁用自动关闭议题

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/19754) in GitLab 12.7.
-->

您可以在[项目设置](../settings/index.md)中按项目禁用自动议题关闭功能。

先决条件：

- 您必须至少具有该项目的维护者角色。

要禁用自动关闭议题：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **默认分支**。
1. 选择 **在默认分支上自动关闭引用的议题**。
1. 选择 **保存修改**。

引用的议题仍会显示，但不会自动关闭。

如果项目禁用了议题跟踪器，则默认情况下在项目中禁用自动议题关闭。<!--如果要启用自动关闭议题，请确保 [启用 GitLab 问题](../settings/index.md#sharing-and-permissions)。-->

更改此设置仅适用于新的合并请求或提交。已经关闭的议题保持原样。如果启用了议题跟踪，则禁用自动关闭议题仅适用于尝试自动关闭同一项目中的议题的合并请求。其他项目中的合并请求仍然可以关闭另一个项目的议题。

<!--
#### 自定义议题关闭 pattern **(FREE SELF)**

先决条件：

- 您必须具有实例的管理员访问级别。

要更改默认议题关闭 pattern，请编辑安装的 [`gitlab.rb` 或 `gitlab.yml` 文件](../../../administration/issue_closure_pattern.md)。
-->

## 更改议题类型

先决条件：

- 您必须是议题作者或至少具有项目的报告者角色。

要更改议题类型：

1. 在标题右侧，选择 **编辑标题和描述** (**{pencil}**)。
1. 编辑议题并从 **议题类型** 下拉列表中选择一个议题类型：

   - Issue
   - Incident<!--[Incident](../../../operations/incident_management/index.md)-->

1. 选择 **保存修改**.

## 删除议题

> 从垂直省略号菜单中删除议题引入于 14.6 版本

先决条件：

- 您必须具有项目的所有者角色。

要删除议题：

1. 在一个议题中，选择垂直省略号 (**{ellipsis_v}**)。
1. 选择 **删除议题**。

或者：

1. 在一个议题中，选择 **编辑标题和描述** (**{pencil}**)。
1. 选择 **删除议题**。

## 将议题提升为史诗 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/3777) in GitLab Ultimate 11.6.
> - Moved to GitLab Premium in 12.8.
> - Promoting issues to epics via the UI [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/233974) in GitLab Premium 13.6.
-->

您可以将议题提升到直接父组中的[史诗](../../group/epics/index.md)。

将议题提升为史诗：

1. 在一个议题中，选择垂直省略号 (**{ellipsis_v}**)。
1. 选择 **提升为史诗**。

或者，您可以使用 `/promote` [快速操作](../quick_actions.md#议题合并请求和史诗)。

<!--
Read more about [promoting an issues to epics](../../group/epics/manage_epics.md#promote-an-issue-to-an-epic).
-->

## 向迭代添加议题 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216158) in GitLab 13.2.
> - Moved to GitLab Premium in 13.9.
-->

将议题添加到[迭代](../../group/iterations/index.md)：

1. 转到议题。
1. 在右侧边栏的 **迭代** 部分，选择 **编辑**。
1. 从下拉列表中，选择要与此议题关联的迭代。
1. 选择下拉列表之外的任何区域。

或者，您可以使用 `/iteration` [快速操作](../quick_actions.md#议题合并请求和史诗)。

## 复制议题引用

要引用实例中其他地方的议题，您可以使用其完整 URL 或简短引用，类似于 `namespace/project-name#123`，其中 `namespace` 是群组或用户名。

要将议题引用复制到剪贴板：

1. 转到议题。
1. 在右侧边栏的 **引用** 旁边，选择 **复制引用** (**{copy-to-clipboard}**)。

您现在可以将引用粘贴到另一个描述或评论中。

<!--
Read more about issue references in [GitLab-Flavored Markdown](../../markdown.md#gitlab-specific-references).
-->

## 复制议题电子邮件地址

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/18816) in GitLab 13.8.
-->

您可以通过发送电子邮件在议题中创建评论。
向此地址发送电子邮件会创建包含电子邮件正文的评论。

<!--
要了解有关通过发送电子邮件和必要配置创建评论的更多信息，请参阅[通过发送电子邮件回复评论](../../discussions/index.md#reply-to-a-comment-by-sending-电子邮件）。
-->

要复制议题的电子邮件地址：

1. 转到议题。
1. 在右侧边栏 **发邮件** 旁边，选择 **复制引用** (**{copy-to-clipboard}**)。

## 实时侧边栏

> - 引入于 13.3 版本。默认禁用。
> - 启用于自助管理版于 14.5 版本。

FLAG:
在自助管理实例上，默认情况下此功能可用。要隐藏每个项目或整个实例的功能，请让管理员[禁用名为 `real_time_issue_sidebar` 和 `broadcast_issue_updates` 功能标志](../../../administration/feature_flags.md)。

侧边栏中的指派人实时更新。

## 指派人

一个问题可以分配给一个或[多个用户](multiple_assignees_for_issues.md)。

指派人可以根据需要随时更改，指派人是对议题负责的人。
当一个议题被分配给某人时，它会出现在他们分配的议题列表中。

如果用户不是项目的成员，则只能将议题分配给他们，前提是他们自己创建议题或其他项目成员将议题分配给他们。

要更改议题的指派人：

1. 转到您的议题。
1. 在右侧边栏的 **指派人** 部分中，选择 **编辑**。
1. 从下拉列表中选择要添加为指派人的用户。
1. 选择下拉列表之外的任何区域。

## 类似议题

为防止同一主题出现重复议题，系统在您创建新议题时会搜索类似议题。

先决条件：

- GraphQL 必须启用。

当您在 **新建议题** 页面的标题文本框中键入时，系统会在当前项目中的所有议题中搜索标题和描述。仅返回您有权访问的议题。
标题文本框下方最多显示五个类似议题，按最近更新排序。

## 健康状态 **(ULTIMATE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/36427) in GitLab 12.10.
> - Health status of closed issues [can't be edited](https://gitlab.com/gitlab-org/gitlab/-/issues/220867) in GitLab 13.4 and later.
> - Issue health status visible in issue lists [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45141) in GitLab 13.6.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/213567) in GitLab 13.7.
-->

为了帮助您跟踪议题状态，您可以为每个议题分配一个状态。
此状态将议题标记为按计划进行，或需要注意以按计划进行。

先决条件：

- 您必须至少具有该项目的报告者角色。

要编辑议题的运行状况：

1. 转到议题。
1. 在右侧边栏的 **健康状态** 部分，选择 **编辑**。
1. 从下拉列表中，选择要添加到此问题的状态：

   - On track (green)
   - Needs attention (amber)
   - At risk (red)

然后，您可以在议题列表和史诗树中查看议题的状态。

议题关闭后，无法编辑其运行状况，并且在重新打开议题之前 **编辑** 按钮将变为禁用状态。

## 发布议题 **(ULTIMATE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/30906) in GitLab 13.1.
-->

如果状态页面应用程序与项目相关联，您可以使用 `/publish` [快速操作](../quick_actions.md) 发布议题。

<!--
For more information, see [GitLab Status Page](../../../operations/incident_management/status_page.md).
-->

## 与议题相关的快速操作

您还可以使用[快速操作](../quick_actions.md#议题合并请求和史诗)来管理议题。

某些操作还没有相应的 UI 按钮。
您可以**仅通过使用快速操作**来执行以下操作：

- [添加或删除 Zoom 会议](associate_zoom_meeting.md)（`/zoom` 和 `/remove_zoom`）。
- [发布议题](#发布议题) (`/publish`)。
- 将议题克隆到同一个或另一个项目 (`/clone`)。
- 关闭一个议题并将其标记为另一个议题的副本（`/duplicate`）。
- 从项目中的另一个合并请求中复制标记和里程碑 (`/copy_metadata`)。