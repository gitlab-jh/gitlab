---
type: reference
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 快速操作 **(FREE)**

<!--
> - Introduced in [GitLab 12.1](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/26672):
>   once an action is executed, an alert appears when a quick action is successfully applied.
> - Introduced in [GitLab 13.2](https://gitlab.com/gitlab-org/gitlab/-/issues/16877): you can use
>   quick actions when updating the description of issues, epics, and merge requests.
> - Introduced in [GitLab 13.8](https://gitlab.com/gitlab-org/gitlab/-/issues/292393): when you enter
>   `/` into a description or comment field, all available quick actions are displayed in a scrollable list.
> - The rebase quick action was [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/49800) in GitLab 13.8.
-->

> - 引入于 13.8 版本：当您在描述或注释字段中输入 `/` 时，所有可用的快速操作都会显示在可滚动列表中。
> - Rebase 快速操作引入于 13.8 版本。

快速操作是常见操作（通常通过选择用户界面中的按钮或下拉菜单来完成）的基于文本的快捷方式。您可以在议题、史诗、合并请求和提交的描述或评论中输入这些命令。

请务必在单独的行上输入每个快速操作，以便正确检测和执行命令。

## 参数

许多快速操作需要一个参数。例如，`/assign` 快速操作需要用户名。极狐GitLab 提供可用值列表，使用自动完成字符<!--[自动完成字符](autocomplete_characters.md)-->和快速操作来帮助用户输入参数。

如果您手动输入参数，则必须将其括在双引号 (`"`) 中，除非它仅包含以下字符：

- ASCII 字母
- 数字（0-9）
- 下划线（`_`），连字符（`-`），问号（`?`），点（`.`）, ＆符号（`&`）或 `@`

参数区分大小写。自动完成会自动处理此问题以及引号的插入。

## 议题、合并请求和史诗

以下快速操作适用于描述、讨论和主题。某些快速操作可能不适用于所有订阅级别。

| 命令                               | 议题                  | 合并请求        | 史诗                  | 操作 |
|:--------------------------------------|:-----------------------|:-----------------------|:-----------------------|:-------|
| `/add_contacts email1 email2`         | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 添加一位或多位 CRM 联系人 (引入于 14.6 版本)。 |
| `/approve`                            | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 核准合并请求。 |
| `/assign @user1 @user2`               | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 指派给多名用户。 |
| `/assign me`                          | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 指派给您自己 |
| `/assign_reviewer @user1 @user2` 或 `/reviewer @user1 @user2` 或 `/request_review @user1 @user2`      | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 指派多名用户作为审核者。 |
| `/assign_reviewer me` 或 `/reviewer me` 或 `/request_review me`                 | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 指派您自己作为审核者。 |
| `/award :emoji:`                      | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 切换表情符号。 |
| `/child_epic <epic>`                  | **{dotted-circle}** No | **{dotted-circle}** No | **{check-circle}** Yes | 将子史诗添加到 `<epic>`。 `<epic>` 值应该采用 `&epic`、`group&epic` 或史诗 URL 的格式。 |
| `/clear_weight`                       | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 清除权重。 |
| `/clone <path/to/project> [--with_notes]`| **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 将议题克隆到给定的项目，如果没有给出参数，则将议题克隆到当前的项目（）。只要目标项目包含等效的标记、里程碑等，就可以复制尽可能多的数据。除非 `--with_notes` 作为参数提供，否则不复制注释或系统注释。 |
| `/close`                              | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 关闭。 |
| `/confidential`                       | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 设置机密。 |
| `/copy_metadata <!merge_request>`     | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 从项目中的另一个合并请求复制标记和里程碑。 |
| `/copy_metadata <#issue>`             | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 从项目中的另一个议题复制标记和里程碑。 |
| `/create_merge_request <branch name>` | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 从当前议题开始创建一个新的合并请求。 |
| `/done`                               | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 标记为完成。 |
| `/draft`                              | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 切换草稿状态。 |
| `/due <date>`                         | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 设定截止日期。有效的 `<date>` 示例包括 `in 2 days`、`this Friday` 和 `December 31st`。                                 |
| `/duplicate <#issue>`                 | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 关闭此议题并将其标记为另一个议题的副本。 **(FREE)** 另外，将两者标记为相关。 |
| `/epic <epic>`                        | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 添加到史诗`<epic>`。`<epic>` 值应该采用 `&epic`、`group&epic` 或史诗 URL 的格式。 |
| `/estimate <time>`                    | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 设置时间估计。 例如，`/estimate 1mo 2w 3d 4h 5m`。<!--Learn more about [time tracking](time_tracking.md).--> |
| `/invite_email email1 email2`         | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 添加最多六个电子邮件参与者。此操作位于功能标志 `issue_email_participants` 之后，议题模板尚不支持。 |
| `/iteration *iteration:"iteration name"`     | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 设置迭代。例如，设置 `Late in July` 迭代：`/iteration *iteration:"Late in July"`。 |
| `/label ~label1 ~label2`              | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 添加一个或多个标记。标记名称也可以不使用波浪号 (`~`) 开头，但不支持混合语法。 |
| `/lock`                               | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 锁定讨论。 |
| `/merge`                              | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 合并更改。根据项目设置，可能是流水线成功时<!--[流水线成功时](merge_requests/merge_when_pipeline_succeeds.md)-->，或添加到合并队列<!--[合并队列](../../ci/pipelines/merge_trains.md)-->时。 |
| `/milestone %milestone`               | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 设置里程碑。 |
| `/move <path/to/project>`             | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 将此议题移至另一个项目。 |
| `/parent_epic <epic>`                 | **{dotted-circle}** No | **{dotted-circle}** No | **{check-circle}** Yes | 将父史诗设置为 `<epic>`。`<epic>` 值的格式应为 `&epic`、`group&epic` 或指向史诗的 URL。 |
| `/promote`                            | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 将议题提升为史诗。 |
| `/promote_to_incident`                | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 将议题提升为事件 (引入于 14.5)。 |
| `/publish`                            | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 将议题发布到关联的状态页面<!--[状态页面](../../operations/incident_management/status_page.md)-->。 |
| `/reassign @user1 @user2`             | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 用指定的指派人替换当前的指派人。 |
| `/rebase`                             | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | Rebase 源分支。系统安排一个后台任务，该任务尝试将源分支中的更改重新设定为目标分支的最新提交。如果使用 `/rebase`，则忽略 `/merge` 以避免出现竞争条件，即在重新定位之前合并或删除源分支。如果存在合并冲突，会显示无法安排 rebase 的消息。变基失败与合并请求状态一起显示。 |
| `/reassign_reviewer @user1 @user2`    | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 用指定的审核者替换当前的审核者。 |
| `/relabel ~label1 ~label2`            | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 用指定的标记替换当前的标记。 |
| `/relate #issue1 #issue2`             | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 将议题标记为相关。 |
| `/remove_child_epic <epic>`           | **{dotted-circle}** No | **{dotted-circle}** No | **{check-circle}** Yes | 从 `<epic>` 中移除子史诗。`<epic>` 值的格式应为 `&epic`、`group&epic` 或指向史诗的 URL。 |
| `/remove_contacts email1 email2`      | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 删除一个或多个 CRM 联系人 (引入于 14.6 版本)。 |
| `/remove_due_date`                    | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 移除截止日期。 |
| `/remove_epic`                        | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 从史诗移除。 |
| `/remove_estimate`                    | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 移除时间估计。|
| `/remove_iteration`                   | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 移除迭代（于 13.1 版本引入）。 |
| `/remove_milestone`                   | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 移除里程碑。 |
| `/remove_parent_epic`                 | **{dotted-circle}** No | **{dotted-circle}** No | **{check-circle}** Yes | 从史诗中删除父史诗。 |
| `/remove_time_spent`                  | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 移除时间花费。 |
| `/remove_zoom`                        | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 从此议题中删除 Zoom 会议。 |
| `/reopen`                             | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 重新开放。 |
| `/severity <severity>`                | **{check-circle}** Yes | **{check-circle}** No  | **{check-circle}** No  | 设置严重性。 `<severity>` 的选项包括 `S1` ... `S4`、`critical`、`high`、`medium`、`low`、`unknown`。引入于 14.2 版本。  |
| `/shrug <comment>`                    | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 附加 `¯\＿(ツ)＿/¯` 到评论。 |
| `/spend <time> [<date>]` | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 增加或减少花费的时间。（可选）指定花费时间的日期。例如，`/spend 1mo 2w 3d 4h 5m 2018-08-26` 或 `/spend -1h 30m`。 <!--Learn more about [time tracking](time_tracking.md).--> |
| `/submit_review`                      | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 提交一个处理中的审核。 |
| `/subscribe`                          | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 订阅通知。 |
| `/tableflip <comment>`                | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 附加 `(╯°□°)╯︵ ┻━┻` 到评论。 |
| `/target_branch <local branch name>`  | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 设置目标分支。 |
| `/title <new title>`                  | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 更改标题。 |
| `/todo`                               | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 添加一个待办事项。 |
| `/unapprove`                          | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 取消批准合并请求（引入于 14.3 版本）。 |
| `/unassign @user1 @user2`             | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 移除指定的指派人。 |
| `/unassign`                           | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 移除所有指派人。 |
| `/unassign_reviewer @user1 @user2` or `/remove_reviewer @user1 @user2`    | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 移除指定的审核者 |
| `/unassign_reviewer` or `/remove_reviewer`                  | **{dotted-circle}** No | **{check-circle}** Yes | **{dotted-circle}** No | 移除所有审核者 |
| `/unlabel ~label1 ~label2` or `/remove_label ~label1 ~label2` | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 移除指定的标记 |
| `/unlabel` or `/remove_label` | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 移除所有标记。 |
| `/unlock`                             | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No | 解锁讨论。 |
| `/unsubscribe`                        | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes | 取消订阅通知。 |
| `/weight <value>`                     | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 设定权重。`<value>` 的有效选项包括 `0`、`1`、`2` 等。 |
| `/zoom <Zoom URL>`                    | **{check-circle}** Yes | **{dotted-circle}** No | **{dotted-circle}** No | 添加 Zoom 会议到此议题。 |

## 提交消息

以下快速操作适用于提交消息：

| 命令                | 操作                                    |
|:----------------------- |:------------------------------------------|
| `/tag v1.2.3 <message>` | 使用可选消息标记提交。 |

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
