---
stage: Manage
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 导出合并请求为 CSV **(FREE)**

> 引入于 13.6 版本。

导出合并请求 CSV，使您和您的团队能够将从合并请求中收集的所有数据导出到逗号分隔值 (CSV) 文件中，该文件以纯文本形式存储表格数据。

要将合并请求导出为 CSV，请从项目的侧栏中导航到您的 **合并请求**，然后单击 **导出到 CSV**。

## CSV 输出

下表显示了 CSV 中将出现的属性。

| 列             | 描述                                                  |
|--------------------|--------------------------------------------------------------|
| MR ID              | MR `iid`                                                     |
| URL                | 极狐GitLab 上的合并请求链接                       |
| Title              | 合并请求标题                                          |
| State              | Opened, Closed, Locked, 或 Merged                            |
| Description        | 合并请求描述                                    |
| Source Branch      | 源分支                                                |
| Target Branch      | 目标分支                                                |
| Source Project ID  | 源项目的 ID                                    |
| Target Project ID  | 目标项目的 ID                                     |
| Author             | 合并请求作者的全名                       |
| Author Username    | 作者的用户名，省略 @ 符号            |
| Assignees          | 合并请求指派人的全名，用 `,` 连接 |
| Assignee Usernames | 指派人的用户名，省略 @ 符号         |
| Approvers          | 核准人的全名，用`,`连接               |
| Approver Usernames | 核准人的用户名，省略 @ 符号        |
| Merged User        | 合并用户的全名                                |
| Merged Username    | 合并用户的用户名，省略 @ 符号        |
| Milestone ID       | 合并请求里程碑的 ID                           |
| Created At (UTC)   | 格式为 `YYYY-MM-DD HH:MM:SS`                           |
| Updated At (UTC)   | 格式为 `YYYY-MM-DD HH:MM:SS`                           |

## 限制

- 将合并请求导出到 CSV 在群组的合并请求列表中不可用。
- 由于合并请求 CSV 文件作为电子邮件附件发送，因此大小限制为 15MB，以确保在一系列电子邮件提供商之间成功交付。如果需要最小化文件大小，可以在导出前缩小搜索范围。例如，您可以在单独的文件中设置导出开放或关闭的合并请求。
