---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 拣选更改 **(FREE)**

极狐GitLab 实现了 Git 的强大功能，可以在合并请求和提交详细信息中使用 **拣选** 按钮[拣选任何提交](https://git-scm.com/docs/git-cherry-pick "Git cherry-pick 文档")。

## 拣选合并请求

合并请求合并后，会显示一个 **拣选** 按钮来拣选该合并请求引入的更改。

![Cherry-pick merge request](img/cherry_pick_changes_mr.png)

单击该按钮后，窗口会显示一个分支过滤搜索框<!--[分支过滤搜索框](../repository/branches/index.md#branch-filter-search-box)-->，您可以在其中选择：

- 直接将更改拣选到选定的分支中。
- 使用拣选的更改创建新的合并请求。

### 跟踪拣选

<!--
> [Introduced](https://gitlab.com/groups/gitlab-org/-/epics/2675) in GitLab 12.9.
-->

当您拣选合并提交时，系统会在相关的合并请求主题显示系统注释，交叉链接新的提交和现有的合并请求。

![Cherry-pick tracking in merge request timeline](img/cherry_pick_mr_timeline_v12_9.png)

每个部署的关联合并请求列表<!--[关联合并请求列表](../../../api/deployments.md#list-of-merge-requests-related-with-a-deployment)-->包括拣选的合并提交。

NOTE:
我们只跟踪从极狐GitLab（UI 和 API）执行的拣选。<!--Support for tracking cherry-picked commits through the command line
is tracked [in this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/202215).-->

## 拣选提交

您可以从提交详细信息页面中拣选一个提交：

![Cherry-pick commit](img/cherry_pick_changes_commit.png)

与拣选合并请求类似，您可以将更改直接拣选到目标分支中，或者创建一个新的合并请求来拣选更改。

当拣选合并提交时，主线始终是第一个父级。如果要使用不同的主线，则需要从命令行执行此操作。

这是一个使用第二个父级作为主线来拣选合并提交的快速示例：

```shell
git cherry-pick -m 2 7a39eb0
```

### 拣选到项目中

> - 引入到 13.11 版本，使用功能标志，默认禁用。
> - 功能标志移除于 14.0 版本。

WARNING:
您可能无法使用此功能。查看上面的**版本历史**注释以了解详细信息。

您可以使用 GitLab UI 将合并请求拣选到项目中，即使合并请求来自一个派生项目：

1. 在合并请求的二级菜单中，点击 **提交**，显示提交详情页面。
1. 单击 **选项** 下拉菜单并选择 **拣选** 以显示拣选窗口。
1. 在 **选择项目** 和 **选择分支** 中，选择目标项目和分支：
   ![Cherry-pick commit](img/cherry_pick_into_project_v13_11.png)
1. （可选）如果您已准备好创建合并请求，请选择 **启动新的合并请求**。
1. 点击 **拣选**。

<!--
## Related links

- The [Commits API](../../../api/commits.md) enables you to add custom messages
  to changes you cherry-pick through the API.
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
