---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 语法高亮 **(FREE)**

极狐GitLab 通过 [Rouge](https://rubygems.org/gems/rouge) Ruby gem 在所有文件上提供语法高亮显示。它试图根据文件扩展名猜测要使用的语言，这在大多数情况下就足够了。

NOTE:
[Web IDE](web_ide/index.md) 和[代码片段](../snippets.md) 使用 [Monaco 编辑器](https://microsoft.github.io/monaco-editor/)进行文本编辑，其中内部使用 [Monarch](https://microsoft.github.io/monaco-editor/monarch.html) 库进行语法高亮显示。

<!-- vale gitlab.Spelling = NO -->

您还可以使用 `.gitattributes` 中的 `gitlab-language` 属性覆盖语言选择。例如，如果您在 Prolog 项目中工作并使用 `.pl` 文件扩展名（通常会突出显示为 Perl），您可以将以下内容添加到您的 `.gitattributes` 文件中：

<!-- vale gitlab.Spelling = YES -->

``` conf
*.pl gitlab-language=prolog
```

<!-- vale gitlab.Spelling = NO -->

当您检入并推送该更改时，您项目中的所有 `*.pl` 文件都会突出显示为 Prolog。

<!-- vale gitlab.Spelling = YES -->

这里的路径是 Git 内置的 [`.gitattributes` 接口](https://git-scm.com/docs/gitattributes)。因此，如果您要在使用 Ruby 语法的项目根目录中发明一种名为 `Nicefile` 的文件格式，您只需要：

``` conf
/Nicefile gitlab-language=ruby
```

要完全禁用突出显示，请使用 `gitlab-language=text`。通过通用网关接口 (CGI) 选项，例如：

``` conf
# json with erb in it
/my-cool-file gitlab-language=erb?parent=json

# an entire file of highlighting errors!
/other-file gitlab-language=text?token=Error
```

这些配置仅在 `.gitattributes` 文件位于您的[默认分支](repository/branches/default.md)中时生效。

NOTE:
Web IDE 不支持`.gitattribute` 文件<!--，但它[计划在未来发布](https://gitlab.com/gitlab-org/gitlab/-/issues/22014)-->。

## 配置用于突出显示的最大文件大小

您可以配置要突出显示的文件的最大大小。

文件大小以千字节为单位，默认设置为 `512 KB`。 任何*超过*文件大小的文件都以纯文本形式呈现。

1. 打开 `gitlab.yml` 配置文件。

1. 添加此部分，将 `maximum_text_highlight_size_kilobytes` 替换为您想要的值。

   ```yaml
   gitlab:
     extra:
       ## Maximum file size for syntax highlighting
       ## https://docs.gitlab.com/ee/user/project/highlighting.html
       maximum_text_highlight_size_kilobytes: 512
   ```
