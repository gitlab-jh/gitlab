---
type: reference
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/time_tracking.html'
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 时间跟踪 **(FREE)**

通过时间跟踪，您可以在极狐GitLab 中跟踪在议题和合并请求上的时间估计和时间花费。

对以下任务使用时间跟踪：

- 记录处理议题或合并请求所花费的时间。
- 添加或更新完成议题或合并请求的总时间估计。
- 查看处理议题或合并请求所花费的时间细分。

您不必指定估计值即可输入花费的时间，反之亦然。

有关时间跟踪的数据显示在议题和合并请求侧栏上：

![Time tracking in the sidebar](img/time_tracking_sidebar_v13_12.png)

## 如何输入数据

时间跟踪使用两个[快速操作](quick_actions.md)：`/spend` 和 `/estimate`。

如果您在一条评论中多次使用任一快速操作，则仅应用最后一次出现的操作。

下面是一个示例，说明如何在评论中使用这些新的快速操作。

![Time tracking example in a comment](img/time_tracking_example_v12_2.png)

添加时间条目（花费的时间或估计的时间）仅适用于具有报告者和更高权限级别<!--[报告者和更高权限级别](../permissions.md)-->的项目成员。

### 估计

要输入估计值，请输入 `/estimate`，然后输入时间。

例如，如果您需要输入 1 个月、2 周、3 天、4 小时和 5 分钟的估计值，请键入 `/estimate 1mo 2w 3d 4h 5m`。
检查[您可以使用的时间单位](#配置)。

估算旨在显示总估算时间。将鼠标悬停在右侧边栏中的时间跟踪信息上时，会自动计算并显示估计的剩余时间。

![Estimated time remaining](img/remaining_time_v14_2.png)

一个议题或一个合并请求只能有一个估计。每次输入新的时间估计值时，它都会覆盖先前的值。

要完全删除估计，请使用 `/remove_estimate`。

### 时间花费

要输入花费的时间，请输入`/spend`，然后输入时间。

例如，如果您需要记录 1 个月、2 周、3 天、4 小时和 5 分钟，请键入 `/spend 1mo 2w 3d 4h 5m`。
检查[您可以使用的时间单位](#配置)。

每个新的花费时间条目都会添加到当前为议题或合并请求花费的总时间。

要减去时间，请输入一个负值。例如，`/spend -3d` 从花费的总时间中删除三天。您花费的时间不能低于 0 分钟，因此如果您删除的时间多于已经输入的时间，系统将忽略减法。

您可以通过在时间之后提供日期来记录过去的时间。
例如，如果您想记录在 2021 年 1 月 31 日花费的 1 小时时间，您可以输入 `/spend 1h 2021-01-31`。如果您提供将来的日期，命令将失败并且不会记录时间。

要一次删除所有花费的时间，请使用 `/remove_time_spent`。

## 查看时间跟踪报告

> 引入于 13.12 版本。

您可以查看在议题或合并请求上花费的时间细分。

先决条件：

- 您必须至少具有项目的报告者角色。

要查看时间跟踪报告，请转到议题或合并请求，然后在右侧边栏中选择 **时间跟踪报告**。

![Time tracking report](img/time_tracking_report_v13_12.png)

花费时间的细分限制为最多 100 个条目。

## 配置

以下时间单位可用：

| 时间单位 | 输入什么 | 默认值 |
| --------- | ------------ | ----------------------- |
| 月     | `mo`         | 4w                      |
| 周      | `w`          | 5d                      |
| 日       | `d`          | 8h                      |
| 小时      | `h`          | 60m                     |
| 分钟    | `m`          |                         |

### 将显示单位限制为小时 **(FREE SELF)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/29469/) in GitLab 12.1.
-->

在自助管理实例中，您可以将时间单位的显示限制为小时。
这样做：

1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > 偏好设置**。
1. 展开 **本地化**。
1. 在 **时间跟踪** 下，选中 **限制时间跟踪单位显示到小时** 复选框。
1. 选择 **保存更改**。

启用此选项后，将显示 `75h` 而不是 `1w 4d 3h`。

<!--
## Related links

- [Time tracking solutions page](https://about.gitlab.com/solutions/time-tracking/)
- Time tracking GraphQL references:
  - [Connection](../../api/graphql/reference/index.md#timelogconnection)
  - [Edge](../../api/graphql/reference/index.md#timelogedge)
  - [Fields](../../api/graphql/reference/index.md#timelog)
  - [Timelogs](../../api/graphql/reference/index.md#querytimelogs)
  - [Group timelogs](../../api/graphql/reference/index.md#grouptimelogs)
  - [Project Timelogs](../../api/graphql/reference/index.md#projecttimelogs)
  - [User Timelogs](../../api/graphql/reference/index.md#usertimelogs)
-->