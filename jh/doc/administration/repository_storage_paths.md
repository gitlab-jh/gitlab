---
stage: Create
group: Gitaly
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 仓库存储 **(FREE SELF)**

极狐GitLab 将仓库<!--[仓库](../user/project/repository/index.md)-->存储在仓库存储中。仓库存储是：

- `gitaly_address`，指向一个 Gitaly 节点<!--[Gitaly 节点](gitaly/index.md)-->。
- `path`，它直接指向存储存储库的目录。此方法在 14.0 版本中已弃用并计划删除。极狐GitLab 应配置为通过 `gitaly_address` 访问 GitLab 仓库。

极狐GitLab 允许您定义多个仓库存储以在多个挂载点之间分配存储负载。例如：

- 使用 Gitaly 时（Omnibus 安装配置）：

  ```ruby
   git_data_dirs({
     'default' => { 'gitaly_address' => 'tcp://gitaly1.internal:8075' },
     'storage2' => { 'gitaly_address' => 'tcp://gitaly2.internal:8075' },
   })
   ```

- 使用直接仓库存储时（源安装配置）：

  ```plaintext
  default:
    gitaly_address: tcp://gitaly1.example:8075
  storage2:
    gitaly_address: tcp://gitaly2.example:8075
  ```

<!--
获取更多信息：

- 配置 Gitaly，查看[相关文档](gitaly/index.md#配置-gitaly)。
- 配置直接存储库访问，请参阅以下部分。
-->
## 配置仓库存储路径

WARNING:
以下信息用于配置 GitLab 以直接访问仓库。此配置选项已弃用，以支持使用 Gitaly<!--[Gitaly](gitaly/index.md)-->，并计划在 14.0 版本中删除。

要配置仓库存储路径：

1. 编辑必要的配置文件：
    - `/etc/gitlab/gitlab.rb`，用于 Omnibus GitLab 安装。
    - `gitlab.yml`，用于从源代码安装。
1. 添加所需的仓库存储路径。

对于仓库存储路径：

- 您必须至少有一个名为 `default` 的存储路径。
- 路径在键值对中定义。 除了 `default`，键可以是您选择的任何名称来命名文件路径。
- 目标目录及其任何子路径不得为符号链接。
- 没有目标目录可能是另一个目录的子目录。也就是说，没有嵌套。例如以下配置无效：

  ```plaintext
  default:
    path: /mnt/git-storage-1
  storage2:
    path: /mnt/git-storage-1/git-storage-2 # <- NOT OK because of nesting
  ```

### 配置备份

要使<!--[备份](../raketasks/backup_restore.md)-->备份正常工作：

- 仓库存储路径不能是挂载点。
- GitLab 用户必须对路径的父目录具有正确的权限。

Omnibus GitLab 会为您处理这些问题，但对于源代码安装，您应该格外小心。

在恢复备份时，`/home/git/repositories` 的当前内容被移动到 `/home/git/repositories.old`。如果 `/home/git/repositories` 是一个挂载点，那么 `mv` 会在挂载点之间移动东西，并且可能会出现问题。

理想情况下，`/home/git` 是挂载点，因此所有内容都保留在同一个挂载点内。Omnibus GitLab 安装保证了这一点，因为它们没有指定完整的仓库路径，而是指定父路径，但源安装没有。

### 示例配置

在下面的示例中，我们添加了两个额外的仓库存储路径，配置为两个额外的挂载点。

出于兼容性原因，`gitlab.yml` 的结构与 Omnibus GitLab 配置不同：

- 在 `gitlab.yml` 中，指定仓库的路径，例如 `/home/git/repositories`
- 在 Omnibus GitLab 配置中，您可以指定 `git_data_dirs`，例如可以是 `/home/git`。然后 Omnibus GitLab 在该路径下创建一个 `repositories` 目录以与 `gitlab.yml` 一起使用。

**源安装实例**

1. 编辑 `gitlab.yml` 并添加存储路径：

   ```yaml
   repositories:
     # Paths where repositories can be stored. Give the canonicalized absolute pathname.
     # NOTE: REPOS PATHS MUST NOT CONTAIN ANY SYMLINK!!!
     storages: # You must have at least a 'default' repository storage path.
       default:
         path: /home/git/repositories
       storage1:
         path: /mnt/storage1/repositories
       storage2:
         path: /mnt/storage2/repositories
   ```

1. [重启极狐GitLab](restart_gitlab.md#源安装实例) 以使更改生效。

1. [配置新仓库的存储位置](#配置新仓库的存储位置)。

**Omnibus 安装实例**

1. 通过将其余路径附加到默认路径来编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   git_data_dirs({
    "default" => { "path" => "/var/opt/gitlab/git-data" },
    "storage1" => { "path" => "/mnt/storage1/git-data" },
    "storage2" => { "path" => "/mnt/storage2/git-data" }
   })
   ```

1. [重启极狐GitLab](restart_gitlab.md#omnibus-安装实例) 使更改生效。

1. [配置新仓库的存储位置](#配置新仓库的存储位置)。

NOTE:
Omnibus 将仓库存储在 `git-data` 目录的 `repositories` 子目录中。

## 配置新仓库的存储位置

[配置](#配置仓库存储路径)多个仓库存储路径后，您可以选择存储新仓库的位置：

1. 在顶部导航栏中，选择 **菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 仓库** 并展开**仓库存储**部分。
1. 在 **新仓库的存储节点** 字段中输入值。
1. 选择 **保存更改**。

可以为每个仓库存储路径分配 0-100 的权重。创建新项目时，这些权重用于确定创建仓库的存储位置。

给定仓库存储路径相对于其他仓库存储路径的权重越高，选择它的频率就越高。公式为：`(storage weight) / (sum of all weights) * 100 = chance %`。

默认情况下，如果之前未配置仓库权重：

- `default` 的权重为 `100`。
- 所有其它存储的权重为 `0`。

NOTE:
如果所有存储权重均为 `0`（例如，当 `default` 不存在时），GitLab 会尝试在 `default` 上创建新的仓库，无论配置如何或是否存在 `default`。

<!--
## 移动仓库

要将仓库移动到不同的仓库存储（例如，从`default` 到`storage2`），请使用与[迁移到 Gitaly 集群](gitaly/index.md#迁移到-gitaly-集群)相同的过程。
-->