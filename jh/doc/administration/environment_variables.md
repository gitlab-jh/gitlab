---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 环境变量 **(FREE SELF)**

极狐GitLab 公开某些可用于覆盖其默认值的环境变量。

人们通常使用 `/etc/gitlab/gitlab.rb` 配置极狐GitLab 以进行 Omnibus 安装，或使用 `gitlab.yml` 配置来自源代码的安装。

您可以使用以下环境变量来覆盖某些值：

## 支持的环境变量

| 变量                                  | 类型   | 描述                                                                                          |
|--------------------------------------------|---------|---------------------------------------------------------------------------------------------------------|
| `DATABASE_URL`                             | string  | 数据库 URL；格式为：`postgresql://localhost/blog_development`。                            |
| `ENABLE_BOOTSNAP`                          | string  | 切换 [Bootsnap](https://github.com/Shopify/bootsnap) 以加快初始 Rails 启动。默认情况下为非生产环境启用。设置为 `0` 以禁用。                                   |
| `EXTERNAL_VALIDATION_SERVICE_TIMEOUT`      | integer | [外部 CI/CD 流水线验证服务](external_pipeline_validation.md)的超时时间，以秒为单位。默认值为 `5`。 |
| `EXTERNAL_VALIDATION_SERVICE_URL`          | string  | [外部 CI/CD 流水线验证服务](external_pipeline_validation.md) 的 URL。                |
| `EXTERNAL_VALIDATION_SERVICE_TOKEN`        | string  | 用于使用[外部 CI/CD 流水线验证服务](external_pipeline_validation.md) 进行身份验证的 `X-Gitlab-Token`。 |
| `GITLAB_CDN_HOST`                          | string  | 设置 CDN 的基本 URL 以提供静态 assets（例如，`//mycdnsubdomain.fictional-cdn.com`）。 |
| `GITLAB_EMAIL_DISPLAY_NAME`                | string  | GitLab 发送的电子邮件中 **From** 字段中使用的名称。                                          |
| `GITLAB_EMAIL_FROM`                        | string  | GitLab 发送的电子邮件中 **From** 字段中使用的电子邮件地址。                                  |
| `GITLAB_EMAIL_REPLY_TO`                    | string  | GitLab 发送的电子邮件中 **Reply-To** 字段中使用的电子邮件地址。                              |
| `GITLAB_EMAIL_SUBJECT_SUFFIX`              | string  | GitLab 发送的电子邮件中使用的电子邮件主题后缀。                                                 |
| `GITLAB_HOST`                              | string  | GitLab 服务器的完整 URL（包括 `http://` 或 `https://`）。                                 |
| `GITLAB_MARKUP_TIMEOUT`                    | string  | 由 `gitlab-markup` gem 执行的 `rest2html` 和 `pod2html` 命令的超时时间，以秒为单位。默认值为 `10`。 |
| `GITLAB_ROOT_PASSWORD`                     | string  | 在安装时设置 `root` 用户的密码。                                                  |
| `GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN` | string  | 设置用于 runner 的初始注册令牌。                                                  |
| `RAILS_ENV`                                | string  | Rails 环境；可以是 `production`、`development`、`staging` 或 `test` 之一。                 |
| `UNSTRUCTURED_RAILS_LOG`                   | string  | 除 JSON 日志外，还启用非结构化日志（默认为 `true`）。                             |

## 完整的数据库变量

The recommended method for specifying your database connection information is
to set the `DATABASE_URL` environment variable. This variable contains
connection information (`adapter`, `database`, `username`, `password`, `host`,
and `port`), but no behavior information (`encoding` or `pool`). If you don't
want to use `DATABASE_URL`, or want to set database behavior information,
either:

指定数据库连接信息的推荐方法是设置`DATABASE_URL` 环境变量。该变量包含连接信息（`adapter`、`database`、`username`、`password`、`host` 和`port`），但不包含行为信息（`encoding` 或`pool`）。如果不想使用 `DATABASE_URL`，或者想设置数据库行为信息，可以：

- 复制模板文件，`cp config/database.yml.env config/database.yml`。
- 为一些 `GITLAB_DATABASE_XXX` 变量设置一个值。

您可以设置的 `GITLAB_DATABASE_XXX` 变量列表是：

| 变量                   | 默认值                 | 被 `DATABASE_URL` 覆盖？ |
|-----------------------------|--------------------------------|-------------------------------|
| `GITLAB_DATABASE_ADAPTER`   | `postgresql`                   | **{check-circle}** Yes        |
| `GITLAB_DATABASE_DATABASE`  | `gitlab_#{ENV['RAILS_ENV']`    | **{check-circle}** Yes        |
| `GITLAB_DATABASE_ENCODING`  | `unicode`                      | **{dotted-circle}** No        |
| `GITLAB_DATABASE_HOST`      | `localhost`                    | **{check-circle}** Yes        |
| `GITLAB_DATABASE_PASSWORD`  |   *无*                         | **{check-circle}** Yes        |
| `GITLAB_DATABASE_POOL`      | `10`                           | **{dotted-circle}** No        |
| `GITLAB_DATABASE_PORT`      | `5432`                         | **{check-circle}** Yes        |
| `GITLAB_DATABASE_USERNAME`  | `root`                         | **{check-circle}** Yes        |

<!--
## 添加更多变量

我们欢迎合并请求，以通过使用变量来配置更多设置。更改 `config/initializers/1_settings.rb` 文件，并使用命名方案 `GITLAB_#{name in 1_settings.rb in upper case}`。
-->

## Omnibus 配置

要设置环境变量，遵循[相关文档说明](https://docs.gitlab.cn/omnibus/settings/environment-variables.html).

<!--
It's possible to preconfigure the GitLab Docker image by adding the environment
variable `GITLAB_OMNIBUS_CONFIG` to the `docker run` command.
For more information, see the [Pre-configure Docker container](https://docs.gitlab.com/omnibus/docker/#pre-configure-docker-container)
section of the Omnibus GitLab documentation.
-->