---
stage: Create
group: Gitaly
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# Praefect Rake 任务 **(FREE SELF)**
<!--
> [Introduced]( https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28369) in GitLab 12.10.
-->

Rake 任务可用于在 Praefect 存储上创建的项目。<!--有关配置 Praefect 的信息，请参阅 [Praefect 文档](../gitaly/praefect.md)。-->

## 副本校验和

`gitlab:praefect:replicas` 打印出给定 `project_id` 的仓库的校验和：

- 主要的 Gitaly 节点。
- 次要的内部 Gitaly 节点。

**Omnibus 安装实例**

```shell
sudo gitlab-rake "gitlab:praefect:replicas[project_id]"
```

**源安装实例**

```shell
sudo -u git -H bundle exec rake "gitlab:praefect:replicas[project_id]" RAILS_ENV=production
```
