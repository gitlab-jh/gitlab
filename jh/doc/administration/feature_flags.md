---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
description: "GitLab administrator: enable and disable GitLab features deployed behind feature flags"
---

# 启用和禁用部署在功能标志后面的极狐GitLab 功能 **(FREE SELF)**

极狐GitLab 采用功能标志策略<!--[功能标志策略](../development/feature_flags/index.md)-->，在开发的早期阶段部署功能，以便它们可以逐步推出。

在使它们永久可用之前，可以出于多种原因将功能部署在标志后面，例如：

- 测试功能。
- 在功能开发的早期阶段从用户和客户那里获得反馈。
- 评估用户的采用程度。
- 评估它如何影响极狐GitLab 的性能。
- 在整个版本中以较小的部分构建它。

标志背后的功能可以逐步推出，通常过程：

1. 该功能默认启动时禁用。
1. 该功能默认启用。
1. 功能标志被移除。

可以启用和禁用这些功能，允许或禁止用户使用它们。可以由具有 GitLab Rails 控制台访问权限的 GitLab 管理员完成。

当您禁用某个功能标志时，该功能将对用户隐藏并且所有功能都将关闭。
例如，不会记录数据并且不会运行服务。

<!--
If you used a certain feature and identified a bug, a misbehavior, or an
error, it's very important that you [**provide feedback**](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issue[title]=Docs%20-%20feature%20flag%20feedback%3A%20Feature%20Name&issue[description]=Describe%20the%20problem%20you%27ve%20encountered.%0A%0A%3C!--%20Don%27t%20edit%20below%20this%20line%20--%3E%0A%0A%2Flabel%20~%22docs%5C-comments%22%20) to GitLab as soon
as possible so we can improve or fix it while behind a flag. When you upgrade
GitLab to an earlier version, the feature flag status may change.
-->

## 启用仍在开发中的功能时的风险

在极狐GitLab 的未来版本中，默认禁用的功能可能会更改或删除，恕不另行通知。

如果启用默认禁用的功能，则可能会发生数据损坏、稳定性下降、性能下降或安全问题。极狐GitLab 支持不涵盖由使用默认禁用功能引起的问题，除非您被极狐GitLab 指示启用该功能。

<!--
在默认情况下，禁用的功能中发现的安全问题在常规版本中进行了修补，并且在向后移植修复程序方面不遵循我们的常规[维护策略](../policy/maintenance.md#security-releases)。 
-->

## 禁用已发布功能时的风险

在大多数情况下，功能标志代码会在未来版本中删除。
如果发生这种情况，从那时起，您将无法将该功能保持在禁用状态。

## 如何启用和禁用标志背后的功能

每个功能都有自己的标志，用于启用和禁用它。

<!--
标志背后的每个功能的文档包括一个部分，用于通知标志的状态和启用或禁用它的命令。
-->

### 启动 GitLab Rails 控制台

您需要启用或禁用标志后面的功能的第一件事是在 GitLab Rails 控制台上启动会话。

对于 Omnibus 安装实例：

```shell
sudo gitlab-rails console
```

对于源安装实例：

```shell
sudo -u git -H bundle exec rails console -e production
```

<!--
For details, see [starting a Rails console session](operations/rails_console.md#starting-a-rails-console-session).
-->

### 启用或禁用功能

Rails 控制台会话启动后，相应地运行 `Feature.enable` 或 `Feature.disable` 命令。可以在该功能的文档本身中找到特定标志。

要启用功能，请运行：

```ruby
Feature.enable(:<feature flag>)
```

例如，要启用名为 `my_awesome_feature` 的虚构功能标志：

```ruby
Feature.enable(:my_awesome_feature)
```

要禁用功能，请运行：

```ruby
Feature.disable(:<feature flag>)
```

例如，要禁用名为 `my_awesome_feature` 的虚构功能标志：

```ruby
Feature.disable(:my_awesome_feature)
```

可以在每个项目的基础上启用或禁用某些功能标志：

```ruby
Feature.enable(:<feature flag>, Project.find(<project id>))
```

例如，要为项目 `1234` 启用 `:product_analytics`<!--[`:product_analytics`](../operations/product_analytics.md#enable-or-disable-product-analytics)--> 功能标志：

```ruby
Feature.enable(:product_analytics, Project.find(1234))
```

`Feature.enable` 和 `Feature.disable` 总是返回 `nil`，这并不表示命令失败：

```ruby
irb(main):001:0> Feature.enable(:my_awesome_feature)
=> nil
```

要检查标志是启用还是禁用，您可以使用 `Feature.enabled?` 或 `Feature.disabled?`。例如，对于名为 `my_awesome_feature` 的虚构功能标志：

```ruby
Feature.enable(:my_awesome_feature)
=> nil
Feature.enabled?(:my_awesome_feature)
=> true
Feature.disabled?(:my_awesome_feature)
=> false
```

当功能准备好时，极狐GitLab 删除功能标志，并且不再存在启用和禁用它的选项。该功能在所有情况下都可用。
