---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
last_update: 2019-07-03
---

# 合并结果流水线 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/7380) in GitLab 11.10.
-->

当您提交合并请求时，您请求将更改从源分支合并到目标分支。默认情况下，CI 流水线针对源分支运行作业。

使用*合并结果的流水线*，流水线运行就好像来自源分支的更改已经合并到目标分支中一样。为流水线显示的提交不存在于源分支或目标分支上，但代表组合的目标分支和源分支。

![Merge request widget for merged results pipeline](img/merged_result_pipeline.png)

如果管道由于目标分支中的问题而失败，您可以等到目标修复后重新运行流水线。
这个新流水线的运行就像源与更新的目标合并一样，您不需要变基。

当目标分支发生变化时，流水线不会自动运行。只有对源分支的更改才会触发新的流水线。如果自上次成功流水线以来已经过去很长时间，您可能需要在合并之前重新运行它，以确保源更改仍然可以成功合并到目标中。

当合并请求无法合并时，流水线仅针对源分支运行。例如：

- 目标分支的更改与源分支中的更改冲突。
- 合并请求是 [**Draft** 合并请求](../../user/project/merge_requests/drafts.md)。

在这些情况下，流水线作为[合并请求的流水线](merge_request_pipelines.md) 运行，并标记为 `detached`。如果这些情况不再存在，新的流水线将再次针对合并的结果运行。

任何拥有开发人员权限的用户都可以运行合并结果的流水线。

## 先决条件

为合并结果启用流水线：

- 您必须具有维护者角色。
- 您必须使用 GitLab Runner 11.9 或更高版本。
- 您不能使用[快进式合并](../../user/project/merge_requests/fast_forward_merge.md)。
- 您的仓库必须是极狐GitLab 仓库，而不是外部仓库<!--[外部仓库](../ci_cd_for_external_repos/index.md)-->。

## 为合并结果启用流水线

要为项目的合并结果启用流水线：

1. [配置您的 CI/CD 配置文件](merge_request_pipelines.md#为合并请求配置流水线)，以便流水线或单个作业针对合并请求运行。
1. 访问您项目的 **设置 > 通用** 并展开**合并请求**。
1. 勾选 **启用合并结果流水线**。
1. 单击 **保存修改**。

WARNING:
如果您选中该复选框但未将 CI/CD 配置为将流水线用于合并请求，您的合并请求可能会卡在未解决状态或您的流水线可能会被丢弃。

## 使用合并队列

当您启用[合并结果的流水线](#合并结果流水线) 时，系统[自动显示](merge_trains.md#向合并队列添加合并请求) **Start/Add Merge Train** 按钮。

通常，这是比立即合并合并请求更安全的选择，因为在实际合并发生之前，您的合并请求会使用预期的合并后结果进行评估。

有关更多信息，请阅读[合并队列文档](merge_trains.md)。

## 自动流水线取消

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/12996) in GitLab 12.3.
-->

GitLab CI/CD 可以检测冗余流水线的存在，并取消它们以节省 CI 资源。

当用户在正在进行的合并队列中立即合并合并请求时，队列将被重建，因为它重新创建了预期的合并后提交和流水线。在这种情况下，合并队列可能已经有针对先前预期的合并后提交运行的流水线。
这些流水线被认为是多余的，会自动取消。

## 故障排查

### 即使将新更改推送到合并请求，也未创建合并结果的流水线

可能是由一些禁用的功能标志引起的。请确保在您的实例上启用了以下功能标志：

- `:merge_ref_auto_sync`

要检查和设置这些功能标志值，请让管理员执行以下操作：

1. 登录实例的 Rails 控制台：

   ```shell
   sudo gitlab-rails console
   ```

1. 检查标志是否启用：

   ```ruby
   Feature.enabled?(:merge_ref_auto_sync)
   ```

1. 如果需要，启用功能标志：

   ```ruby
   Feature.enable(:merge_ref_auto_sync)
   ```

<!--
### 间歇性流水线因 `fatal: reference is not a tree:` 错误失败

由于合并结果的流水线是在合并请求 (`refs/merge-requests/<iid>/merge`) 的合并引用上运行的，因此 Git 引用可能会在意外的时间被覆盖。例如，当源或目标分支被推进时。在这种情况下，流水线由于 `fatal: reference is not a tree:` 错误而失败，这表明在合并引用中找不到 checkout-SHA。

This behavior was improved at GitLab 12.4 by introducing [Persistent pipeline refs](../troubleshooting.md#fatal-reference-is-not-a-tree-error).
You should be able to create pipelines at any timings without concerning the error.
-->