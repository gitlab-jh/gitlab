---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 可以使用变量的地方 **(FREE)**

正如 [CI/CD 变量](index.md) 文档中所述，您可以定义许多不同的变量。其中一些可用于所有 GitLab CI/CD 功能，但其中一些或多或少受到限制。

本文档描述了可以在何处以及如何使用不同类型的变量。

## 变量使用

有两个地方可以使用定义的变量。 在：

1. GitLab 端，在 `.gitlab-ci.yml` 中。
1. GitLab Runner 端，在 `config.toml` 中。

### `.gitlab-ci.yml` 文件

| 定义                                | 可以扩展吗？ | 扩展位置        | 描述                                                                                                                                                                                                                                                                                                                                                                                                                       |
|:-------------------------------------------|:-----------------|:-----------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `environment:url`                          | yes              | GitLab                 | 变量扩展是通过 GitLab 中的[内部变量扩展机制](#gitlab-内部变量扩展机制)进行的。<br/><br/>支持为作业定义的所有变量（项目/群组变量、来自 `.gitlab-ci.yml` 的变量、来自触发器的变量、来自流水线计划的变量）。<br/><br/>不支持在 GitLab Runner `config.toml` 中定义的变量和在作业的 `script` 中创建的变量。 |
| `environment:name`                         | yes              | GitLab                 | 类似于 `environment:url`，但变量扩展不支持以下内容：<br/><br/>- 基于环境名称的变量（`CI_ENVIRONMENT_NAME`、`CI_ENVIRONMENT_SLUG`）。<br/>- 与环境相关的任何其他变量（目前只有`CI_ENVIRONMENT_URL`）。<br/>- [持久变量](#持久变量)。                                                                               |
| `resource_group`                           | yes              | GitLab                 | 类似于 `environment:url`，但变量扩展不支持以下内容：<br/>- `CI_ENVIRONMENT_URL`<br/>- [持久变量](#持久变量)                                                                                |
| `include`                                  | yes              | GitLab                 | 变量扩展是通过 GitLab 中的[内部变量扩展机制](#gitlab-内部变量扩展机制)进行的。 <br/><br/>支持预定义的项目变量：`GITLAB_FEATURES`、`CI_DEFAULT_BRANCH`，以及所有以`CI_PROJECT_` 开头的变量（例如`CI_PROJECT_NAME`）。                                                                                                            |
| `variables`                                | yes              | GitLab/Runner          | 变量扩展首先由 GitLab 中的[内部变量扩展机制](#gitlab-内部变量扩展机制)进行，然后任何无法识别或不可用的变量由 GitLab Runner 的[内部变量扩展机制](#gitlab-runner-内部变量扩展机制)扩展。                                                                                                   |
| `image`                                    | yes              | Runner                 | 变量扩展是通过 GitLab Runner 中的[内部变量扩展机制](#gitlab-runner-内部变量扩展机制)进行的。                                                                                                                                                                                                                                                                                   |
| `services:[]`                              | yes              | Runner                 | 变量扩展是通过 GitLab Runner 中的[内部变量扩展机制](#gitlab-runner-内部变量扩展机制)进行的。                                                                                                                                                                                                                                                                                  |
| `services:[]:name`                         | yes              | Runner                 | 变量扩展是通过 GitLab Runner 中的[内部变量扩展机制](#gitlab-runner-内部变量扩展机制)进行的。                                                                                                                                                                                                                                                                                   |
| `cache:key`                                | yes              | Runner                 | 变量扩展是通过 GitLab Runner 中的[内部变量扩展机制](#gitlab-runner-内部变量扩展机制)进行的。                                                                                                                                                                                                                                                                                   |
| `artifacts:name`                           | yes              | Runner                 | 变量扩展由 GitLab Runner 的 shell 环境进行                                                                                                                                                                                                                                                                                                                                                               |
| `script`, `before_script`, `after_script`  | yes              | Script execution shell | 变量扩展由 [execution shell 环境](#执行-shell-环境)进行                                                                                                                                                                                                                                                                                                                                |
| `only:variables:[]`, `except:variables:[]`, `rules:if` | no   | n/a                    | 变量必须采用 `$variable` 的形式。不支持的有以下几种：<br/><br/>- 基于环境名称的变量（`CI_ENVIRONMENT_NAME`、`CI_ENVIRONMENT_SLUG`）。<br/>- 任何其他与环境相关的变量（目前只有`CI_ENVIRONMENT_URL`）。<br/>- [持久变量](#持久变量)。                                                                                        |

### `config.toml` 文件

| 定义                          | 可以扩展吗？ | 描述                                                                                                                                  |
|:-------------------------------------|:-----------------|:---------------------------------------------------------------------------------------------------------------------------------------------|
| `runners.environment`                | yes              | 变量扩展是通过 GitLab Runner 中的[内部变量扩展机制](#gitlab-runner-内部变量扩展机制)进行的。 |
| `runners.kubernetes.pod_labels`      | yes              | 变量扩展是通过 GitLab Runner 中的[内部变量扩展机制](#gitlab-runner-内部变量扩展机制)进行的。 |
| `runners.kubernetes.pod_annotations` | yes              | 变量扩展是通过 GitLab Runner 中的[内部变量扩展机制](#gitlab-runner-内部变量扩展机制)进行的。 |

<!--
You can read more about `config.toml` in the [GitLab Runner docs](https://docs.gitlab.com/runner/configuration/advanced-configuration.html).
-->

## 扩展机制

存在三种扩展机制：

- GitLab
- GitLab Runner
- 执行 shell 环境

### GitLab 内部变量扩展机制

展开的部分需要是 `$variable`，或者 `${variable}` 或者 `%variable%` 的形式。
无论哪个 OS/shell 处理作业，每个表单都以相同的方式处理，因为扩展是在任何 runner 获得作业之前在 GitLab 中完成的。

#### 嵌套变量扩展

- 引入于 13.10 版本。部署在 `variable_inside_variable` 功能标志后默认禁用。
- 适用于自助管理版于 14.4 版本。
- 功能标志 `variable_inside_variable` 移除于 14.5 版本。

GitLab 在将作业变量值发送给 runner 之前递归地扩展它们。例如，在以下场景中：

```yaml
- BUILD_ROOT_DIR: '${CI_BUILDS_DIR}'
- OUT_PATH: '${BUILD_ROOT_DIR}/out'
- PACKAGE_PATH: '${OUT_PATH}/pkg'
```

Runner 收到一个有效的、完全成形的路径。例如，如果 `${CI_BUILDS_DIR}` 是 `/output`，那么 `PACKAGE_PATH` 就是 `/output/out/pkg`。

对不可用变量的引用保持不变。在这种情况下，runner 在运行时[尝试扩展变量值](#gitlab-runner-内部变量扩展机制)。
例如，像 `CI_BUILDS_DIR` 这样的变量只有在运行时才被 runner 知道。

### GitLab Runner 内部变量扩展机制

- 支持：项目/群组变量、`.gitlab-ci.yml` 变量、`config.toml` 变量以及来自触发器、计划流水线和手动流水线的变量。
- 不支持：在脚本内部定义的变量（例如，`export MY_VARIABLE="test"`）。

Runner 使用 Go 的 `os.Expand()` 方法进行变量扩展。这意味着它只处理定义为 `$variable` 和 `${variable}` 的变量。同样重要的是，扩展只进行一次，因此嵌套变量可能会也可能不会起作用，这取决于变量定义的顺序，以及是否在 GitLab 中启用了[嵌套变量扩展](#嵌套变量扩展)。

### 执行 shell 环境

这是在 `script` 执行期间发生的扩展阶段。
它的行为取决于使用的 shell（`bash`、`sh`、`cmd`、PowerShell）。例如，如果作业的 `script` 包含一行 `echo $MY_VARIABLE-${MY_VARIABLE_2}`，它应该由 bash/sh 正确处理（根据变量是否定义，留下空字符串或一些值），但是不适用于 Windows 的 `cmd` 或 PowerShell，因为这些 shell 使用不同的变量语法。

支持：

- `script` 可以使用 shell 默认的所有可用变量（例如，应该存在于所有 bash/sh shell 中的 `$PATH`）和 GitLab CI/CD 定义的所有变量（项目/群组变量，`.gitlab-ci.yml` 变量、`config.toml` 变量以及来自触发器和计划流水线的变量）。
- `script` 也可以使用之前行中定义的所有变量。因此，例如，如果您定义一个变量 `export MY_VARIABLE="test"`：
  - 在 `before_script` 中，它在 `before_script` 的后续行和相关 `script` 的所有行中起作用。
  - 在 `script` 中，它在 `script` 的后续行中起作用。
  - 在 `after_script` 中，它在 `after_script` 的后续行中起作用。

对于 `after_script` 脚本，它们可以：

- 仅使用在同一 `after_script` 部分中的脚本之前定义的变量。
- 不使用在 `before_script` 和 `script` 中定义的变量。

之所以存在这些限制，是因为 `after_script` 脚本是在[分离的 shell 上下文](../yaml/index.md#after_script)中执行的。

## 持久变量

以下变量是“持久化的”：

- `CI_PIPELINE_ID`
- `CI_JOB_ID`
- `CI_JOB_TOKEN`
- `CI_JOB_STARTED_AT`
- `CI_BUILD_ID`
- `CI_BUILD_TOKEN`
- `CI_REGISTRY_USER`
- `CI_REGISTRY_PASSWORD`
- `CI_REPOSITORY_URL`
- `CI_DEPLOY_USER`
- `CI_DEPLOY_PASSWORD`

它们：

- 支持["扩展位置"](#gitlab-ciyml-文件)的定义是：
  - Runner。
  - Script execution shell。
- 不支持：
  - 对于 ["扩展位置"](#gitlab-ciyml-文件) 的定义是 GitLab。
  - 在 `only`、`except` 和 `rules` 变量表达式<!--[变量表达式](../jobs/job_control.md#cicd-variable-expressions)-->中。

一些持久化变量包含令牌，由于安全原因不能被某些定义使用。

## 具有环境范围的变量

支持使用环境范围定义的变量。鉴于在 `review/staging/*` 范围内定义了一个变量 `$STAGING_SECRET`，基于匹配的变量表达式创建以下使用动态环境的作业：

```yaml
my-job:
  stage: staging
  environment:
    name: review/$CI_JOB_STAGE/deploy
  script:
    - 'deploy staging'
  rules:
    - if: $STAGING_SECRET == 'something'
```
