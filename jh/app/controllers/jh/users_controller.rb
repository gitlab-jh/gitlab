# frozen_string_literal: true

module JH
  module UsersController
    extend ::Gitlab::Utils::Override

    def phone_exists
      render json: { exists: ::UserDetail.find_by_phone(params[:phone]).present? }
    end

    private

    override :user
    def user
      return if action_name == "phone_exists"

      super
    end
  end
end
