# frozen_string_literal: true

module JH
  # User JH mixin
  #
  # This module is intended to encapsulate JH-specific model logic
  # and be prepended in the  model

  module Note
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      include ContentValidateable
      validates :note, content_validation: true, if: :with_project_should_validate_content?

      private

      def with_project_should_validate_content?
        return false if system?

        super
      end
    end
  end
end
