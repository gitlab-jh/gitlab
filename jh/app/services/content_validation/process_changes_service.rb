# frozen_string_literal: true

module ContentValidation
  class ProcessChangesService
    # The N most recent commits to process in a single change payload.
    PROCESS_COMMIT_LIMIT = 100

    def initialize(container:, project:, repo_type:, user:, changes:)
      @container = container
      @project = project
      @repo_type = repo_type
      @user = user
      @changes = changes
      @repository = container.repository
    end

    def execute
      return false unless ::ContentValidation::Setting.check_enabled?(@container)
      return false unless @user

      if @repo_type.project?
        return false if @project.private?
      elsif @repo_type.wiki?
        wiki = @container
        return false if wiki.container.private?
      elsif @repo_type.snippet?
        snippet = @container
        return false if snippet.private?
      end

      @changes.each do |change|
        # rubocop:disable Style/ConditionalAssignment
        if creating_default_branch?(change)
          commits = @repository.commits(change[:newrev], limit: PROCESS_COMMIT_LIMIT).reverse!
        elsif creating_ref?(change)
          commits = @repository.commits_between(@container.default_branch, change[:newrev], limit: PROCESS_COMMIT_LIMIT)
        elsif updating_ref?(change)
          commits = @repository.commits_between(change[:oldrev], change[:newrev], limit: PROCESS_COMMIT_LIMIT)
        else # removing ref
          commits = []
        end
        # rubocop:enable Style/ConditionalAssignment

        commits.each do |commit|
          ContentValidation::CommitServiceWorker.perform_async(commit.id, container_identifier, @user.id)
        end
      end
    end

    private

    def container_identifier
      @container_identifier ||= @repo_type.identifier_for_container(@container)
    end

    def creating_ref?(change)
      Gitlab::Git.blank_ref?(change[:oldrev]) || !@repository.ref_exists?(change[:ref])
    end

    def creating_default_branch?(change)
      creating_ref?(change) && default_branch?(change)
    end

    def default_branch?(change)
      return false unless Gitlab::Git.branch_ref?(change[:ref])

      Gitlab::Git.branch_name(change[:ref]) == @container.default_branch
    end

    def updating_ref?(change)
      !creating_ref?(change) && !removing_ref?(change)
    end

    def removing_ref?(change)
      Gitlab::Git.blank_ref?(change[:newrev])
    end
  end
end
