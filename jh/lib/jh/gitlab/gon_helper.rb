# frozen_string_literal: true

module JH
  module Gitlab
    module GonHelper
      extend ::Gitlab::Utils::Override

      override :add_gon_variables
      def add_gon_variables
        super

        gon.recaptcha_enabled = ::Gitlab::CurrentSettings.recaptcha_enabled
        gon.tencent_captcha_enabled = true
        gon.tencent_captcha_app_id = ENV['TC_CAPTCHA_APP_ID']
      end
    end
  end
end
