# frozen_string_literal: true

module JH
  module Gitlab
    module Patch
      module DrawRoute
        extend ::Gitlab::Utils::Override

        RoutesNotFound = ::Gitlab::Patch::DrawRoute::RoutesNotFound

        override :draw
        def draw(routes_name)
          drawn_any = draw_jh(routes_name) | draw_ee(routes_name) | draw_ce(routes_name)

          drawn_any || raise(RoutesNotFound, "Cannot find #{routes_name}")
        end

        def draw_jh(routes_name)
          draw_route(route_path("jh/config/routes/#{routes_name}.rb"))
        end
      end
    end
  end
end
