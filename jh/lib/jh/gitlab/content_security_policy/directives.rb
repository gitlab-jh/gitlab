# frozen_string_literal: true

module JH
  module Gitlab
    module ContentSecurityPolicy
      module Directives
        extend ActiveSupport::Concern

        class_methods do
          extend ::Gitlab::Utils::Override

          override :frame_src
          def frame_src
            super + " https://*.qq.com/ https://captcha.gtimg.com"
          end

          override :script_src
          def script_src
            super + " https://*.qq.com/ https://captcha.gtimg.com"
          end
        end
      end
    end
  end
end
